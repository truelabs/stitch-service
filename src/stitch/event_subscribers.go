package stitch

import (
	"errors"
	"github.com/drichardson/gcm"
	"golang.org/x/net/context"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/urlfetch"
)

// API Key in Google Developer Console > Credentials:
// https://console.developers.google.com/project/stitch-1026/apiui/credential
const gcmAPIKey = "AIzaSyBmeKRuaNN_K3WUhcBe13lhR0sWfyEjC9k"

type EventSubscriber struct {
	EventID  string
	DeviceID string
}

const eventSubscriberKind = "EventSubscriber"

func eventSubscriberKey(c context.Context, eventID, deviceID string) (*datastore.Key, error) {
	keyString := eventID + "/" + deviceID
	key := datastore.NewKey(c, eventSubscriberKind, keyString, 0, nil)
	if key.Incomplete() {
		return nil, errors.New("Incomplete event subscriber key.")
	}
	return key, nil
}

func PutEventSubscriber(c context.Context, eventID, deviceID string) error {
	key, err := eventSubscriberKey(c, eventID, deviceID)
	if err != nil {
		return err
	}
	_, err = datastore.Put(c, key, &EventSubscriber{EventID: eventID, DeviceID: deviceID})
	return err
}

func DeleteEventSubscriber(c context.Context, eventID, deviceID string) error {
	key, err := eventSubscriberKey(c, eventID, deviceID)
	if err != nil {
		return err
	}
	return datastore.Delete(c, key)
}

func minInt(a int, b int) int {
	if a < b {
		return a
	}
	return b
}

func NotifyEventSubscribers(c context.Context, eventID, deviceID string) error {
	query := datastore.NewQuery(eventSubscriberKind).Filter("EventID =", eventID)
	iterator := query.Run(c)
	var subscriberDeviceKeys []*datastore.Key
	for {
		var subscriber EventSubscriber
		_, err := iterator.Next(&subscriber)
		if err == datastore.Done {
			break
		}
		if err != nil {
			log.Errorf(c, "Fetching next subscriber: %v", err)
			return err
		}
		deviceKey, err := DeviceKeyForID(c, subscriber.DeviceID)
		if err != nil {
			log.Errorf(c, "Didn't get key for ID %v. %v", subscriber.DeviceID, err)
			return err
		}
		subscriberDeviceKeys = append(subscriberDeviceKeys, deviceKey)
	}

	subscriberDevices := make([]Device, len(subscriberDeviceKeys))
	err := datastore.GetMulti(c, subscriberDeviceKeys, subscriberDevices)
	if err != nil {
		return err
	}

	var tokens []string
	for _, device := range subscriberDevices {
		tokens = append(tokens, device.DeviceToken)
	}

	client := urlfetch.Client(c)
	data := map[string]interface{}{
		"op":        "event_updated",
		"event_id":  eventID,
		"device_id": deviceID,
	}
	sender := &gcm.Sender{ApiKey: gcmAPIKey, Http: client}

	// Send in batches of 1000 (the limit defined by GCM).
	const batchSize = 1000
	const numRetry = 10
	for i := 0; i < len(tokens); i += batchSize {
		tokenBatch := tokens[i:minInt(len(tokens), i+batchSize)]
		message := gcm.NewMessage(data, tokenBatch...)
		message.ContentAvailable = true
		response, err := sender.Send(message, numRetry)
		if err != nil {
			log.Errorf(c, "Error sending message to Google Cloud Messaging. %v", err)
			return err
		}

		for _, r := range response.Results {
			log.Debugf(c, "GCM response: error=%v, messageID=%v, registrationID=%v", r.Error, r.MessageID, r.RegistrationID)
		}
	}

	return nil
}
