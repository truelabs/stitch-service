package test

import (
	"net/http"
	"testing"
)

func TestCaptures(t *testing.T) {
	t.Parallel()

	//c := LogTransport(http.DefaultClient, t)
	//defer c.RestoreTransport()

	// Need device owners.
	auth := RegisterNewDevice(t)

	// Create event for the captures
	type jsonMap map[string]interface{}
	event := jsonMap{"name": "My Event", "owner": auth.DeviceId, "anyone_can_post": false}
	eventId := NewTestId()
	res := PutAuthJson(auth, EventURL(eventId), event)
	if res.StatusCode != http.StatusNoContent {
		t.Fatalf("Expected ok no content, got %v", res.StatusCode)
	}

	contentMD5 := "c983cd57397745ca5521311cecc153b7" // value doesn't matter for tests

	// The put capture URL should only be accessible via signed URLs.
	// Try without signed request, this should fail.
	res = PutEmpty(CaptureURL(eventId, auth.DeviceId, contentMD5))
	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Expected bad request, got %v", res.StatusCode)
	}
	// and should also fail if your authenticated (because it's still not signed).
	res = PutAuthEmpty(auth, CaptureURL(eventId, auth.DeviceId, contentMD5))
	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Expected bad request, got %v", res.StatusCode)
	}

	// Try without auth, this should fail.
	res = PutEmpty(UploadCompleteURL(eventId, auth.DeviceId, contentMD5))
	if res.StatusCode != http.StatusForbidden {
		t.Fatalf("Expected forbidden, got %v", res.StatusCode)
	}

	// Try posting with the event owner, with a valid request.
	res = PutAuthEmpty(auth, UploadCompleteURL(eventId, auth.DeviceId, contentMD5))
	if res.StatusCode != http.StatusNoContent {
		t.Fatalf("Expected no content, got %v", res.StatusCode)
	}
}
