package test

import (
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
)

func TestDevices(t *testing.T) {
	t.Parallel()

	r := Get(DeviceURL("not-important"))
	if r.StatusCode != http.StatusMethodNotAllowed {
		t.Errorf("Get unexpectedly allowed. %v", r.StatusCode)
	}

	putDeviceWithPasswordOfLength(t, 0, http.StatusBadRequest)
	putDeviceWithPasswordOfLength(t, 7, http.StatusBadRequest)
	putDeviceWithPasswordOfLength(t, 65, http.StatusBadRequest)
	putDeviceWithPasswordOfLength(t, 8, http.StatusNoContent)
	putDeviceWithPasswordOfLength(t, 64, http.StatusNoContent)

	id := NewTestId()
	password1 := "12345678"
	username := "tester"
	PutDeviceRequire(t, id, password1, username, http.StatusNoContent)
	PutDeviceRequire(t, id, password1, username, http.StatusNoContent)             // idempotent
	PutDeviceRequire(t, id, password1+"different", username, http.StatusNoContent) // can change password
	PutDeviceRequire(t, id, password1, username+"1", http.StatusNoContent)         // can change username

	PutDeviceRequire(t, NewTestId(), NewTestId(), "", http.StatusNoContent) // empty username ok
}

func putDeviceWithPasswordOfLength(t *testing.T, length int, expectedStatus int) {
	PutDeviceExpect(t, NewTestId(), strings.Repeat("a", length), "testerPasswordLength", expectedStatus)
}

func PutDeviceExpect(t *testing.T, deviceId, password, username string, expectedStatus int) {
	r := PutDevice(t, deviceId, password, username)
	if r.StatusCode != expectedStatus {
		body, _ := ioutil.ReadAll(r.Body)
		t.Errorf("PutDevice failed. Expected %v but got %v. Body: %v", expectedStatus, r.StatusCode, string(body))
	}
}
