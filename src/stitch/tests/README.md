# Stitch Tests

Because stitch is using google.golang.org/appengine instead of the classic
appengine package, aetest is not available. That means tests need to be run
against the local development server (started with `goapp serve`) or against
a deployed server.

Therefore, all tests need to be in terms of HTTP requests, and not aetest style
unit tests that can access the stitch Go implementation directly. That means these
are more like integration tests than unit tests. But they should be lightweight so
that developers will run them often. Longer running tests should go elsewhere.

## Running the Tests

To run against deployed version:

    go test -apibase="https://stitch-1026.appspot.com"


To run against local server started with `goapp serve`:

    go test -apibase="http://localhost:8080"

or simply:

    go test
