package test

import (
	"net/http"
	"testing"
)

func TestEvents(t *testing.T) {
	t.Parallel()

	sc := Get(EventURL(NewTestId())).StatusCode
	if sc != http.StatusNotFound {
		t.Errorf("Expected not found, got %v", sc)
	}

	// Need device owners.
	auth := RegisterNewDevice(t)
	authDevice2 := RegisterNewDevice(t)

	type jsonMap map[string]interface{}

	event := jsonMap{"name": "My Event", "owner": auth.DeviceId}

	// First try to create an event without authenticating
	sc = PutJson(EventURL(NewTestId()), event).StatusCode
	if sc != http.StatusForbidden {
		t.Fatalf("Expected forbidden, got %v", sc)
	}

	// Now try with authentication, but not with your own device ID.
	res := PutAuthJson(auth, EventURL(NewTestId()), jsonMap{"name": "My Event", "owner": authDevice2.DeviceId})
	if res.StatusCode != http.StatusForbidden {
		t.Fatalf("Expected forbidden, got %v", res.StatusCode)
	}

	// Now try authenticating, with an event you actually do own. This should work.
	eventId := NewTestId()
	res = PutAuthJson(auth, EventURL(eventId), event)
	if res.StatusCode != http.StatusNoContent {
		t.Fatalf("Expected ok no content, got %v", res.StatusCode)
	}

	BackoffRetry(t, func(t Tester) {
		// Make sure you can retrieve the event.
		sc = Get(EventURL(eventId)).StatusCode
		if sc != http.StatusOK {
			t.Fatalf("Expected ok, got %v", sc)
		}
	})

	// Test event locations.
	loc := jsonMap{"latitude": 0.1, "longitude": 0.1}
	// not the owner, forbidden
	res = PutAuthJson(authDevice2, EventLocationURL(eventId), loc)
	if res.StatusCode != http.StatusForbidden {
		t.Fatalf("Expected forbidden, got %v", res.StatusCode)
	}
	// is the owner, allowed
	res = PutAuthJson(auth, EventLocationURL(eventId), loc)
	if res.StatusCode != http.StatusNoContent {
		t.Fatalf("Expected no content, got %v", res.StatusCode)
	}

	BackoffRetry(t, func(t Tester) {
		found := false
		locs := GetJsonArray(EventsNearURL(0.1, 0.1))
		for _, loc := range locs {
			l, ok := loc.(map[string]interface{})
			if !ok {
				t.Fatal("Expected dictionary.")
			}
			id, ok := l["id"].(string)
			if !ok {
				t.Fatal("Expected string id")
			}
			if id == eventId {
				found = true
				break
			}
		}

		if !found {
			t.Fatalf("Didn't find expected event")
		}
	})
}
