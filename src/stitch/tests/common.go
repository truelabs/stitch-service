package test

import (
	"bytes"
	"code.google.com/p/go-uuid/uuid"
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"testing"
)

var apibase = flag.String("apibase", "http://localhost:8080", "URL base of API server")

func BaseURL() *url.URL {
	u, err := url.Parse(*apibase)
	if err != nil {
		panic("Couldn't parse apibase flag '" + *apibase + "' as URL. " + err.Error())
	}
	return u
}

func HttpDo(req *http.Request) *http.Response {
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}
	return res
}

func Get(u *url.URL) *http.Response {
	r, err := http.Get(u.String())
	if err != nil {
		panic(err)
	}
	return r
}

func GetAuth(auth *Auth, u *url.URL) *http.Response {
	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		panic(err)
	}
	auth.SetHeaders(req)
	r, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}
	return r
}

func GetString(u *url.URL) (string, *http.Response) {
	r := Get(u)
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	return string(b), r
}

func PutEmpty(u *url.URL) *http.Response {
	r, err := http.NewRequest("PUT", u.String(), nil)
	if err != nil {
		panic(err)
	}
	return HttpDo(r)
}

func Put(u *url.URL, contentType string, body io.Reader) *http.Response {
	r, err := http.NewRequest("PUT", u.String(), body)
	r.Header.Set("Content-Type", contentType)
	if err != nil {
		panic(err)
	}
	return HttpDo(r)
}

func PutWithHeaders(u *url.URL, header map[string]string, body io.Reader) *http.Response {
	r, err := http.NewRequest("PUT", u.String(), body)
	for k, v := range header {
		r.Header.Set(k, v)
	}
	if err != nil {
		panic(err)
	}
	return HttpDo(r)
}

func DoAuth(method string, auth *Auth, u *url.URL, contentType string, body io.Reader) *http.Response {
	r, err := http.NewRequest(method, u.String(), body)
	if err != nil {
		panic(err)
	}
	if contentType != "" {
		r.Header.Set("Content-Type", contentType)
	}
	auth.SetHeaders(r)
	return HttpDo(r)
}

func PostAuth(auth *Auth, u *url.URL, contentType string, body io.Reader) *http.Response {
	return DoAuth("POST", auth, u, contentType, body)
}

func PutAuth(auth *Auth, u *url.URL, contentType string, body io.Reader) *http.Response {
	return DoAuth("PUT", auth, u, contentType, body)
}

func PutAuthEmpty(auth *Auth, u *url.URL) *http.Response {
	return PutAuth(auth, u, "", nil)
}

func PutString(u *url.URL, contentType string, data string) *http.Response {
	return Put(u, contentType, strings.NewReader(data))
}

func PutAuthString(auth *Auth, u *url.URL, contentType string, data string) *http.Response {
	return PutAuth(auth, u, contentType, strings.NewReader(data))
}

func PutJson(u *url.URL, v interface{}) *http.Response {
	return Put(u, JsonMimeType, JsonReader(v))
}

func getJsonInternal(auth *Auth, u *url.URL, result interface{}) {
	var r *http.Response
	if auth != nil {
		r = GetAuth(auth, u)
	} else {
		r = Get(u)
	}
	if r.StatusCode != http.StatusOK {
		panic(fmt.Sprintf("Expected status ok got %v", r.StatusCode))
	}
	d := json.NewDecoder(r.Body)
	err := d.Decode(result)
	if err != nil {
		panic(err)
	}
}

func GetJson(u *url.URL) map[string]interface{} {
	m := make(map[string]interface{})
	getJsonInternal(nil, u, &m)
	return m
}

func GetJsonArray(u *url.URL) []interface{} {
	var a []interface{}
	getJsonInternal(nil, u, &a)
	return a
}

func GetAuthJson(auth *Auth, u *url.URL) map[string]interface{} {
	m := make(map[string]interface{})
	getJsonInternal(auth, u, &m)
	return m
}

type Auth struct {
	DeviceId, DevicePassword string
}

func (auth *Auth) SetHeaders(r *http.Request) {
	r.Header.Set("Device-Id", auth.DeviceId)
	r.Header.Set("Device-Password", auth.DevicePassword)
}

func PutAuthJson(auth *Auth, u *url.URL, v interface{}) *http.Response {
	return PutAuth(auth, u, JsonMimeType, JsonReader(v))
}

func PostAuthJson(auth *Auth, u *url.URL, v interface{}) *http.Response {
	return PostAuth(auth, u, JsonMimeType, JsonReader(v))
}

const JsonMimeType = "application/json"

func JsonReader(v interface{}) io.Reader {
	b, err := json.Marshal(v)
	if err != nil {
		panic(err)
	}
	return bytes.NewReader(b)
}

func DeviceURL(id string) *url.URL {
	u := BaseURL()
	u.Path = "/devices/" + id
	return u
}

func EventURL(id string) *url.URL {
	u := BaseURL()
	u.Path = "/events/" + id
	return u
}

func EventLocationURL(id string) *url.URL {
	u := EventURL(id)
	u.Path += "/location"
	return u
}

func EventsNearURL(latitude, longitude float64) *url.URL {
	s := fmt.Sprintf("%v/events?filter=near&latitude=%v&longitude=%v", BaseURL().String(), latitude, longitude)
	u, err := url.Parse(s)
	if err != nil {
		panic(err)
	}
	return u
}

func CaptureURLBase(eventId, deviceId, contentMD5 string) *url.URL {
	u := EventURL(eventId)
	u.Path += "/captures/" + deviceId + "/" + contentMD5
	return u
}

func CaptureURL(eventId, deviceId, contentMD5 string) *url.URL {
	u := CaptureURLBase(eventId, deviceId, contentMD5)
	u.Path += "/capture"
	return u
}

func DataURL(eventId, deviceId, contentMD5 string) *url.URL {
	u := CaptureURLBase(eventId, deviceId, contentMD5)
	u.Path += "/data"
	return u
}

func UploadDataURL(eventId, deviceId, contentMD5, contentType string) *url.URL {
	q := fmt.Sprintf("/datauploadurl?contenttype=%v", contentType)
	raw := CaptureURLBase(eventId, deviceId, contentMD5).String() + q
	u, err := url.Parse(raw)
	if err != nil {
		panic(err)
	}
	return u
}

func UploadCompleteURL(eventId, deviceId, contentMD5 string) *url.URL {
	u := CaptureURLBase(eventId, deviceId, contentMD5)
	u.Path += "/uploadcomplete"
	return u
}

func MakeMovieURL(eventId string) *url.URL {
	u := EventURL(eventId)
	u.Path += "/makemovie"
	return u
}

type logTransport struct {
	rt               http.RoundTripper
	t                *testing.T
	client           *http.Client
	previousClientRt http.RoundTripper
}

type TransportRestorer interface {
	RestoreTransport()
}

func (lt *logTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	lt.t.Logf("RoundTrip: %v %v", req.Method, req.URL)
	res, err := lt.rt.RoundTrip(req)
	var code int
	if res != nil {
		code = res.StatusCode
	}
	lt.t.Logf("RoundTrip: status=%v, err=%v", code, err)
	return res, err
}

func LogTransport(client *http.Client, t *testing.T) TransportRestorer {
	rt := http.DefaultTransport
	if client.Transport != nil {
		rt = client.Transport
	}
	lt := &logTransport{client: client, rt: rt, t: t, previousClientRt: client.Transport}
	client.Transport = lt
	return lt
}

func (t *logTransport) RestoreTransport() {
	if t.client == nil {
		panic("No client wrapped. Did you try to Unwrap more than once?")
	}
	t.client.Transport = t.previousClientRt
	t.client = nil // so we panic if this is called again
}

func NewTestId() string {
	// prefix all IDs with 'test-' so you can tell which ones are test IDs
	// if you run tests against production.
	return "test-" + uuid.New()
}

func NewAuth() *Auth {
	return &Auth{DeviceId: NewTestId(), DevicePassword: NewTestId()}
}

func RegisterNewDevice(t *testing.T) *Auth {
	auth := NewAuth()
	PutDeviceRequire(t, auth.DeviceId, auth.DevicePassword, "tester", http.StatusNoContent)
	return auth
}

func PutDeviceRequire(t *testing.T, deviceId, password, username string, expectedStatus int) {
	r := PutDevice(t, deviceId, password, username)
	if r.StatusCode != expectedStatus {
		body, _ := ioutil.ReadAll(r.Body)
		t.Fatalf("PutDevice failed. Expected %v but got %v. Body: %v", expectedStatus, r.StatusCode, string(body))
	}
}

func PutDevice(t *testing.T, deviceId, password, username string) *http.Response {
	v := make(map[string]string)
	v["password"] = password
	v["username"] = username
	return PutJson(DeviceURL(deviceId), v)
}

func ComputeMD5(r io.Reader) []byte {
	h := md5.New()
	if _, err := io.Copy(h, r); err != nil {
		panic(err)
	}
	return h.Sum(nil)
}

// returns the hex encoded content md5
func Upload(t *testing.T, auth *Auth, eventId, contentType string, reader io.ReadSeeker) string {
	if _, err := reader.Seek(0, 0); err != nil {
		t.Fatal("Failed to seek to beginning before computing md5.", err)
	}

	contentMD5 := ComputeMD5(reader)
	contentMD5Hex := hex.EncodeToString(contentMD5)
	contentMD5Base64 := base64.StdEncoding.EncodeToString(contentMD5)

	if _, err := reader.Seek(0, 0); err != nil {
		t.Fatal("Failed to seek to beginning before uploading.", err)
	}

	// authenticated, my bucket. All is good.
	v := GetAuthJson(auth, UploadDataURL(eventId, auth.DeviceId, contentMD5Hex, contentType))
	dataURLStr := v["url"].(string)
	dataURL, err := url.Parse(dataURLStr)
	if err != nil {
		t.Fatalf("Failed to parse %v. Error: %v", dataURLStr, err)
	}

	// put data into bucket, using a custom content type
	headers := map[string]string{"Content-Type": contentType, "Content-MD5": contentMD5Base64}
	res := PutWithHeaders(dataURL, headers, reader)
	if res.StatusCode != http.StatusOK {
		t.Log("This failure may be due to not using a real service account to run your local development server. If so, run ./serve.sh in the stitch application directory and follow the instructions to create a service account.")
		t.Fatalf("Expected ok, got %v", res.StatusCode)
	}

	return contentMD5Hex
}
