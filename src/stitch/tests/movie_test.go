package test

import (
	"net/http"
	"testing"
)

func TestMovies(t *testing.T) {
	t.Parallel()

	//c := LogTransport(http.DefaultClient, t)
	//defer c.RestoreTransport()

	// Need device owners.
	auth := RegisterNewDevice(t)

	// Create event for the captures
	type jsonMap map[string]interface{}
	event := jsonMap{"name": "My Event", "owner": auth.DeviceId, "anyone_can_post": false}
	eventId := NewTestId()
	if r := PutAuthJson(auth, EventURL(eventId), event); r.StatusCode != http.StatusNoContent {
		t.Fatalf("Expected ok no content, got %v", r.StatusCode)
	}

	// Upload two videos and one image in parallel to reduce test time.
	md5Hex := make(chan string)
	go func() { md5Hex <- Upload(t, auth, eventId, "video/mp4", Base64Reader(MP4Video1Base64)) }()
	go func() { md5Hex <- Upload(t, auth, eventId, "video/mp4", Base64Reader(MP4Video2Base64)) }()
	go func() { md5Hex <- Upload(t, auth, eventId, "image/jpeg", Base64Reader(JPEGImage1Base64)) }()

	md5Hexes := make([]string, 0)
	for i := 0; i < 3; i++ {
		md5Hexes = append(md5Hexes, <-md5Hex)
	}

	captures := make([]map[string]interface{}, 0)
	for _, h := range md5Hexes {
		capture := map[string]interface{}{
			"owner":       auth.DeviceId,
			"content_md5": h,
		}
		captures = append(captures, capture)
	}

	j := map[string]interface{}{
		"captures": captures,
	}
	if r := PostAuthJson(auth, MakeMovieURL(eventId), j); r.StatusCode != http.StatusNoContent {
		t.Fatal("Expected no content, got", r.StatusCode)
	}
}

// TestMovieSpecificEvent is meant for testing out the moviemaker service, not
// for general unit testing. Whoever uses this next will likely have to hand
// edit the hard coded string IDs below.
func DISABLEDTestMovieSpecificEvent(t *testing.T) {
	auth := RegisterNewDevice(t)

	eventId := "97fc903f-362c-4f54-b849-e55ee8c2679c"
	ownerId := "821c607d-91d6-44de-9c75-2c0b3f738127"

	v := []struct {
		owner      string
		contentMd5 string
	}{
		{ownerId, "1496d9856c7270592f6121474cb5156b"},
		{ownerId, "44271e1b79eecea709dcceb333e6c2cb"},
		{ownerId, "79ea9c5f0f584bfa6a3fee7c80715dbc"},
		{ownerId, "7d6adf5e58a243ac8cfd379518b7070e"},
		{ownerId, "b3c287bd8d3eb09999dccbadd5320925"},
		{ownerId, "b87de9e1be9bd744fda5c94e8d7929d3"},
		{ownerId, "dd11d31451f00a6d13b178fd49aa6c2b"},
	}

	captures := make([]map[string]interface{}, 0)
	for _, h := range v {
		capture := map[string]interface{}{
			"owner":       h.owner,
			"content_md5": h.contentMd5,
		}
		captures = append(captures, capture)
	}
	j := map[string]interface{}{
		"captures": captures,
	}
	if r := PostAuthJson(auth, MakeMovieURL(eventId), j); r.StatusCode != http.StatusNoContent {
		t.Fatal("Expected no content, got", r.StatusCode)
	}
}
