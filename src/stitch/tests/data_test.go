package test

import (
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"net/http"
	"net/url"
	"strings"
	"testing"
)

func TestData(t *testing.T) {
	t.Parallel()

	c := LogTransport(http.DefaultClient, t)
	defer c.RestoreTransport()

	// Need device owners.
	auth := RegisterNewDevice(t)

	// Get non-existent data.
	data := "abc123"
	eventId := NewTestId()
	md5Sum := md5.Sum([]byte(data))
	contentMd5 := hex.EncodeToString(md5Sum[:])
	t.Logf("Content MD5 is %v", contentMd5)
	contentType := "application/stitch-json" // use a custom content type to make sure no auto-detection happens.

	sc := Get(DataURL(eventId, auth.DeviceId, contentMd5)).StatusCode
	if sc != http.StatusNotFound {
		t.Fatalf("Expected not found, got %v", sc)
	}
	// not authenticated
	sc = Get(UploadDataURL(eventId, auth.DeviceId, contentMd5, contentType)).StatusCode
	if sc != http.StatusForbidden {
		t.Fatalf("Expected forbidden, got %v", sc)
	}
	// authenticated, my bucket. All is good.
	v := GetAuthJson(auth, UploadDataURL(eventId, auth.DeviceId, contentMd5, contentType))
	dataUrlStr := v["url"].(string)
	dataUrl, err := url.Parse(dataUrlStr)
	if err != nil {
		t.Fatalf("Failed to parse %v. Error: %v", dataUrlStr, err)
	}
	// put data into bucket, using a custom content type
	md5Bytes, err := hex.DecodeString(contentMd5)
	if err != nil {
		panic(err)
	}
	contentMd5Base64 := base64.StdEncoding.EncodeToString(md5Bytes)
	headers := map[string]string{"Content-Type": contentType, "Content-MD5": contentMd5Base64}
	res := PutWithHeaders(dataUrl, headers, strings.NewReader(data))
	if res.StatusCode != http.StatusOK {
		t.Log("This failure may be due to not using a real service account to run your local development server. If so, run ./serve.sh in the stitch application directory and follow the instructions to create a service account.")
		t.Fatalf("Expected ok, got %v", res.StatusCode)
	}

	res = PutAuthEmpty(auth, UploadCompleteURL(eventId, auth.DeviceId, contentMd5))
	if res.StatusCode != http.StatusNoContent {
		t.Fatalf("Expected ok, got %v", res.StatusCode)
	}

	// Make sure all user's have their own namespace. Using the same data id for two
	// different device IDs is fine. This is necessary because data IDs are publically known
	// and we don't want someone to hi-jack another users upload just because they no the
	// data id.

	headers2 := map[string]string{"Content-Type": contentType + "2", "Content-MD5": contentMd5Base64}

	auth2 := RegisterNewDevice(t)
	v = GetAuthJson(auth2, UploadDataURL(eventId, auth2.DeviceId, contentMd5, contentType+"2"))
	dataUrlStr2 := v["url"].(string)
	dataUrl2, err := url.Parse(dataUrlStr2)
	if err != nil {
		t.Fatalf("Failed to parse %v. Error: %v", dataUrlStr, err)
	}
	// put data into bucket, using a custom content type
	res = PutWithHeaders(dataUrl2, headers2, strings.NewReader(data))
	if res.StatusCode != http.StatusOK {
		t.Log("This failure may be due to not using a real service account to run your local development server. If so, run ./serve.sh in the stitch application directory and follow the instructions to create a service account.")
		t.Fatalf("Expected ok, got %v", res.StatusCode)
	}

	// Get data you just put. Each device should have it's own data, even though they
	// used the same dataId.
	responseString, res := GetString(DataURL(eventId, auth.DeviceId, contentMd5))
	if res.StatusCode != http.StatusOK {
		t.Fatalf("Expected ok, but %v", sc)
	}
	returnedContentType := res.Header.Get("Content-Type")
	if contentType != returnedContentType {
		t.Errorf("Expected custom content type, got %v", returnedContentType)
	}
	if responseString != data {
		t.Fatalf("Expected '%v' but got '%v'", data, responseString)
	}

	responseString2, res := GetString(DataURL(eventId, auth2.DeviceId, contentMd5))
	if res.StatusCode != http.StatusOK {
		t.Fatalf("Expected ok, but %v", sc)
	}
	returnedContentType2 := res.Header.Get("Content-Type")
	if contentType+"2" != returnedContentType2 {
		t.Errorf("Expected custom content type, got %v", returnedContentType2)
	}
	if responseString2 != data {
		t.Fatalf("Expected '%v' but got '%v'", data+"2", responseString)
	}

}
