package test

import (
	"math"
	"runtime"
	"testing"
	"time"
)

type Tester interface {
	Error(args ...interface{})
	Errorf(format string, args ...interface{})
	Fail()
	FailNow()
	Failed() bool
	Fatal(args ...interface{})
	Fatalf(format string, args ...interface{})
	Log(args ...interface{})
	Logf(format string, args ...interface{})
	Skip(args ...interface{})
	SkipNow()
	Skipf(format string, args ...interface{})
	Skipped() bool
}

type testWrapper struct {
	anyFailures bool
	skipped     bool
	result      chan bool
}

func (tw *testWrapper) Error(args ...interface{}) {
	tw.Fail()
}

func (tw *testWrapper) Errorf(format string, args ...interface{}) {
	tw.Fail()
}

func (tw *testWrapper) Fail() {
	tw.anyFailures = true
}

func (tw *testWrapper) FailNow() {
	tw.anyFailures = true
	tw.result <- false
	runtime.Goexit()
}

func (tw *testWrapper) Failed() bool {
	return tw.anyFailures
}

func (tw *testWrapper) Fatal(args ...interface{}) {
	tw.FailNow()
}

func (tw *testWrapper) Fatalf(format string, args ...interface{}) {
	tw.FailNow()
}

func (tw *testWrapper) Log(args ...interface{}) {
}

func (tw *testWrapper) Logf(format string, args ...interface{}) {
}

func (tw *testWrapper) Skip(args ...interface{}) {
	tw.SkipNow()
}

func (tw *testWrapper) SkipNow() {
	tw.skipped = true
	tw.result <- tw.anyFailures
	runtime.Goexit()
}

func (tw *testWrapper) Skipf(format string, args ...interface{}) {
	tw.SkipNow()
}

func (tw *testWrapper) Skipped() bool {
	return tw.skipped
}

func runTry(f func(t Tester)) bool {
	result := make(chan bool)
	// run the test inside a Goroutine so it can call runtime.Goexit
	// to abort the test without taking down the Goroutine that called
	// this function.
	go func() {
		tw := &testWrapper{result: result}
		f(tw)
		// if we make it here, runtime.Goexit hasn't been called and
		// the channel value hasn't been sent, so do that now.
		result <- !tw.Failed()
	}()
	return <-result
}

func RetryN(t *testing.T, n int, f func(t Tester)) {
	for i := 1; i < n; i++ {
		t.Logf("Running retry %v", i)
		if runTry(f) {
			t.Logf("Retry %v succeeded.", i)
			return
		}
	}

	// On the nth time, try with the real testing.T so that any errors are logged.
	f(t)
}

func BackoffRetryN(t *testing.T, n int, minDelay, maxDelay time.Duration, f func(t Tester)) {
	delay := minDelay
	for i := 1; i < n; i++ {
		t.Logf("Running retry %v", i)
		if runTry(f) {
			t.Logf("Retry %v succeeded.", i)
			return
		}
		t.Logf("sleeping %v...", delay)
		time.Sleep(delay)
		delay = time.Duration(math.Min(float64(minDelay)*math.Pow(2, float64(i)), float64(maxDelay)))
	}

	// On the nth time, try with the real testing.T so that any errors are logged.
	f(t)
}

func BackoffRetry(t *testing.T, f func(t Tester)) {
	BackoffRetryN(t, 10, 10*time.Millisecond, 1*time.Second, f)
}
