package stitch

import (
	"bitbucket.org/truelabs/httphelp"
	"encoding/json"
	"fmt"
	"github.com/drichardson/appengine/googleapiclient"
	"github.com/drichardson/appengine/signedrequest"
	dstorage "github.com/drichardson/appengine/storage"
	"github.com/julienschmidt/httprouter"
	"golang.org/x/net/context"
	"google.golang.org/api/googleapi"
	storage "google.golang.org/api/storage/v1"
	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/delay"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/taskqueue"
	"io/ioutil"
	"net/http"
	"runtime"
	"strconv"
	"strings"
	"time"
)

type authenticatedHandle func(http.ResponseWriter, *http.Request, httprouter.Params, AuthenticatedContext)

// authenticateDeviceHandler verifies the Device-Id and Device-Password headers
// are valid.
func authenticateDeviceHandler(handler authenticatedHandle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		c := appengine.NewContext(r)
		id := r.Header.Get("Device-Id")
		password := r.Header.Get("Device-Password")
		if !AuthenticateDevice(c, id, password) {
			httphelp.RespondForbidden(w)
			return
		}
		context := NewAuthenticatedDeviceContext(id)
		handler(w, r, p, context)
	}
}

// signedRequestHandle is like http.Handle, but also takes the signed headers as the last
// parameter. Headers that do not appear in the signed headers argument are not signed.
type signedRequestHandle func(http.ResponseWriter, *http.Request, httprouter.Params, http.Header)

// signedRequestHandler verifies the URL was signed by the stitch application
// and has not expired.
func signedRequestHandler(handler signedRequestHandle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		c := appengine.NewContext(r)
		callback, err := signedrequest.ParseHTTPRequest(r)
		if err != nil {
			log.Debugf(c, "Couldn't parse signed HTTP request. %v", err)
			httphelp.RespondBadRequest(w, "Not a well formed signed request. "+err.Error())
			return
		}
		err = callback.Verify(c)
		if err != nil {
			if err == signedrequest.ErrExpired {
				httphelp.RespondBadRequest(w, "Signed url expired.")
			} else {
				respondWithError(c, w, err)
			}
			return
		}
		handler(w, r, p, callback.Headers)
	}
}

type cronHandle func(http.ResponseWriter, *http.Request, httprouter.Params)

func cronHandler(handle cronHandle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		// From https://cloud.google.com/appengine/docs/go/config/cron
		// The X-Appengine-Cron header is set internally by Google App Engine. If your request
		// handler finds this header it can trust that the request is a cron request. If the
		// header is present in an external user request to your app, it is stripped, except
		// for requests from logged in administrators of the application,
		// who are allowed to set the header for testing purposes.
		if r.Header.Get("X-Appengine-Cron") != "true" {
			httphelp.RespondForbidden(w)
			return
		}

		handle(w, r, p)
	}
}

func getEvents(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

	c := appengine.NewContext(r)
	filter := r.FormValue("filter")

	var events []*Event
	var err error

	if filter == "near" {
		lat, err := httphelp.ParseFloat64FromRequest(r, "latitude")
		if err != nil {
			httphelp.RespondBadRequest(w, err.Error())
			return
		}
		long, err := httphelp.ParseFloat64FromRequest(r, "longitude")
		if err != nil {
			httphelp.RespondBadRequest(w, err.Error())
			return
		}

		searchRadiusInMeters := 1000.0
		eventIDs, err := GetMostRecentEventsIDsNear(c, appengine.GeoPoint{Lat: lat, Lng: long}, searchRadiusInMeters)
		if err == ErrInvalidInput {
			httphelp.RespondBadRequest(w, err.Error())
			return
		}
		log.Debugf(c, "Got %v event IDs.", len(eventIDs))

		log.Debugf(c, "TODO: BATCH REQUEST FOR EVENTS")

		for _, id := range eventIDs {
			event, err := GetEventByID(c, id)
			if err != nil {
				log.Debugf(c, "GetEventByID failed. Skipping. %v", err.Error())
				continue
			}
			events = append(events, event)
		}
	} else {
		httphelp.RespondBadRequest(w, "Invalid filter")
		return
	}

	if err != nil {
		respondWithError(c, w, err)
		return
	}

	httphelp.RespondJSON(w, r, events)
}

// SingleEventResponse contains an Event and all its Captures.
type SingleEventResponse struct {
	Event    *Event        `json:"event"`
	Captures []CaptureJSON `json:"captures"`
}

// CaptureJSON is a JSON version of Capture. It uses string IDs instead of datastore.Keys
// and client versions of the Thumbnail and Video structs.
type CaptureJSON struct {
	Owner       string          `json:"owner"`
	OwnerName   string          `json:"owner_name"`
	ContentMD5  string          `json:"content_md5"`
	ContentType string          `json:"content_type"`
	Time        time.Time       `json:"time"`
	Thumbnails  []ThumbnailJSON `json:"thumbnails"`
	Transcodes  []VideoJSON     `json:"transcodes"`
}

// ThumbnailJSON is a JSON version of Thumbnail for client devices (as opposed to task queue payloads).
// It uses publicaly readable URLs instead of BucketObjects to identify the thumbnail location.
type ThumbnailJSON struct {
	Width  int    `json:"width"`
	Height int    `json:"height"`
	URL    string `json:"url"`
}

// VideoJSON is a JSON version of Video specifically for client devices (as opposed to
// task queue payloads).
type VideoJSON struct {
	ContentType   string `json:"content_type"`
	Width         int    `json:"width"`
	Height        int    `json:"height"`
	BitsPerSecond int    `json:"bits_per_second"`
	URL           string `json:"url"`
}

func copyCaptureToCaptureJSON(src *Capture, dest *CaptureJSON) {
	dest.Owner = src.OwnerKey.StringID()
	dest.ContentMD5 = src.ContentMD5
	dest.Time = src.Time
	dest.ContentType = src.ContentType

	for _, thumbnail := range src.Thumbnails {
		thumbnailJSON := ThumbnailJSON{
			Width:  thumbnail.Width,
			Height: thumbnail.Height,
			URL:    thumbnail.Object.PublicGetURL(),
		}
		dest.Thumbnails = append(dest.Thumbnails, thumbnailJSON)
	}

	for _, video := range src.Transcodes {
		videoJSON := VideoJSON{
			ContentType:   video.ContentType,
			Width:         video.Width,
			Height:        video.Height,
			BitsPerSecond: video.BitsPerSecond,
			URL:           video.Object.PublicGetURL(),
		}
		dest.Transcodes = append(dest.Transcodes, videoJSON)
	}
}

func getEvent(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	eventID := ps.ByName("event_id")
	c := appengine.NewContext(r)
	event, err := GetEventByID(c, eventID)
	if err != nil {
		httphelp.RespondNotFound(w)
		return
	}

	captures, err := GetCapturesByEventID(c, eventID)
	if err != nil {
		respondWithError(c, w, err)
		return
	}

	captureJSONs := make([]CaptureJSON, len(captures))
	for i, v := range captures {
		copyCaptureToCaptureJSON(&v, &captureJSONs[i])
	}

	//
	// Now get a map of owner ID to user ID.
	//

	// Collect all owner IDs.
	ownerMap := make(map[string]string, len(captureJSONs))
	for _, capture := range captureJSONs {
		ownerMap[capture.Owner] = ""
	}
	ownerIDs := make([]string, 0, len(ownerMap))
	for k, _ := range ownerMap {
		ownerIDs = append(ownerIDs, k)
	}
	devices, err := GetDevicesByIDs(c, ownerIDs)
	if err != nil {
		log.Errorf(c, "GetDevicesByIDs failed with %v", err)
		httphelp.RespondInternalServerError(w)
		return
	}
	for _, device := range devices {
		ownerMap[device.ID] = device.Username
	}
	for i := 0; i < len(captureJSONs); i++ {
		captureJSON := &captureJSONs[i]
		username, ok := ownerMap[captureJSON.Owner]
		if ok {
			captureJSON.OwnerName = username
		} else {
			log.Warningf(c, "No username for owner %v", captureJSON.Owner)
		}
	}

	result := SingleEventResponse{Captures: captureJSONs, Event: event}
	httphelp.RespondJSON(w, r, result)
}

func putEvent(w http.ResponseWriter, r *http.Request, ps httprouter.Params, context AuthenticatedContext) {
	eventID := ps.ByName("event_id")

	decoder := json.NewDecoder(r.Body)
	var event Event
	err := decoder.Decode(&event)
	if err != nil {
		httphelp.RespondBadRequest(w, "Invalid JSON: "+err.Error())
		return
	}

	event.Name = strings.TrimSpace(event.Name)
	if event.Name == "" {
		httphelp.RespondBadRequest(w, "Invalid event name.")
		return
	}

	// Can only create events for yourself.
	if event.Owner != context.AuthenticatedDeviceID() {
		httphelp.RespondForbidden(w)
		return
	}

	event.ID = eventID
	c := appengine.NewContext(r)
	err = event.Put(c)
	if err == ErrForbidden {
		httphelp.RespondForbidden(w)
		return
	} else if err != nil {
		respondWithError(c, w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// EventLocationJSON defines the latitude and longitude for an event.
type EventLocationJSON struct {
	Latitude  float64 `json="latitude"`
	Longitude float64 `json="longitude"`
}

func putEventLocation(w http.ResponseWriter, r *http.Request, ps httprouter.Params, context AuthenticatedContext) {
	eventID := ps.ByName("event_id")

	var loc EventLocationJSON
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&loc)
	if err != nil {
		httphelp.RespondBadRequest(w, "Invalid JSON: "+err.Error())
		return
	}

	c := appengine.NewContext(r)
	event, err := GetEventByID(c, eventID)
	if err != nil {
		respondWithError(c, w, err)
		return
	}

	if event.Owner != context.AuthenticatedDeviceID() {
		httphelp.RespondForbidden(w)
		return
	}

	err = PutEventLocation(c, eventID, appengine.GeoPoint{Lat: loc.Latitude, Lng: loc.Longitude})
	if err != nil {
		respondWithError(c, w, err)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func putEventSubscriber(w http.ResponseWriter, r *http.Request, ps httprouter.Params, context AuthenticatedContext) {
	eventID := ps.ByName("event_id")
	deviceID := ps.ByName("device_id")

	if context.AuthenticatedDeviceID() != deviceID {
		httphelp.RespondForbidden(w)
		return
	}

	c := appengine.NewContext(r)
	err := PutEventSubscriber(c, eventID, deviceID)
	if err != nil {
		respondWithError(c, w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func deleteEventSubscriber(w http.ResponseWriter, r *http.Request, ps httprouter.Params, context AuthenticatedContext) {
	eventID := ps.ByName("event_id")
	deviceID := ps.ByName("device_id")

	if context.AuthenticatedDeviceID() != deviceID {
		httphelp.RespondForbidden(w)
		return
	}

	c := appengine.NewContext(r)
	err := DeleteEventSubscriber(c, eventID, deviceID)
	if err != nil {
		respondWithError(c, w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func fetchContentType(c context.Context, bucket, objectName string) (string, error) {
	client := googleapiclient.NewClient(c, storage.DevstorageReadOnlyScope)
	storageService, err := storage.New(client)
	if err != nil {
		log.Errorf(c, "Couldn't get storage client. %v", err)
		return "", err
	}
	object, err := storageService.Objects.Get(bucket, objectName).Fields("contentType").Do()
	if err != nil {
		log.Errorf(c, "Couldn't get object. %v", err)
		return "", err
	}
	return object.ContentType, nil
}

func putUploadComplete(w http.ResponseWriter, r *http.Request, ps httprouter.Params, context AuthenticatedContext) {
	eventID := ps.ByName("event_id")
	deviceID := ps.ByName("device_id")
	contentMD5 := ps.ByName("content_md5")

	if context.AuthenticatedDeviceID() != deviceID {
		httphelp.RespondForbidden(w)
		return
	}

	source := &dstorage.BucketObject{
		Bucket: DefaultStorageBucket.Name,
		Object: storageObjectName(eventID, deviceID, contentMD5),
	}

	c := appengine.NewContext(r)

	contentType, err := fetchContentType(c, source.Bucket, source.Object)
	if err != nil {
		log.Errorf(c, "Error fetching content type for %v", source)
		httphelp.RespondInternalServerError(w)
		return
	}

	if strings.HasPrefix(contentType, "image/") {
		// This is an image, can go directly to thumbnail generation.
		log.Debugf(c, "Image, going to phase2")
		err = phase2ScheduleThumbnailJob(c, source, eventID, deviceID, contentMD5)
		if err != nil {
			log.Errorf(c, "Error scheduling thumbnail job. %v", err)
			httphelp.RespondInternalServerError(w)
			return
		}
	} else if strings.HasPrefix(contentType, "video/") {
		log.Debugf(c, "Video, going to phase1")
		err = phase1PrepareVideo(c, source, eventID, deviceID, contentMD5)
		if err != nil {
			log.Errorf(c, "Error scheduling video prepatation job. %v", err)
			httphelp.RespondInternalServerError(w)
			return
		}
	} else {
		log.Errorf(c, "Unexpected content type %v. Ignoring capture.", contentType)
		httphelp.RespondBadRequest(w, "Uploaded object has invalid content type "+contentType)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func phase1PrepareVideo(c context.Context, source *dstorage.BucketObject, eventID, deviceID, contentMD5 string) error {
	hostname, err := CurrentModuleVersionHostname(c)
	if err != nil {
		log.Errorf(c, "Couldn't get module hostname. %v", err)
		return err
	}

	callbackURL := fmt.Sprintf("https://%v/events/%v/captures/%v/%v/transcodecallback", hostname, eventID, deviceID, contentMD5)

	callback := signedrequest.SignedRequest{
		Method:     "POST",
		URL:        callbackURL,
		Expiration: time.Now().Add(24 * time.Hour),
	}
	err = callback.Sign(c)
	if err != nil {
		log.Errorf(c, "Failed to sign url. %v", err)
		return err
	}

	transcodeParameters := []struct {
		MaxEdge int
		BitRate int
	}{
		// ExtractVideoFrameOperation takes advantage of the fact the smallest
		// bitrate appears first in this slice. Make sure that remains true
		// if you modify this slice.
		{640, 1200000},
		{1280, 4000000},
	}

	var jobs []*TranscodeOperationsRequest
	for _, transcodeParams := range transcodeParameters {
		job := &TranscodeOperationsRequest{
			Source: *source,
			VideoTranscodeOperations: []VideoTranscodeOperation{
				NewVideoTranscodeOperation(source, transcodeParams.MaxEdge, transcodeParams.BitRate),
			},
			Callback: callback,
		}
		jobs = append(jobs, job)
	}

	// Include video frame extraction operation in the first op. This assumes the
	// first op will have the lowest bitrate, and thus complete the quickest. By coupling
	// the video extract operation with the cheapest transcode, the entire set of transcodes
	// should finish no slower (since total transcode time will be dominated by the highest
	// bitrate encoding). Additionally, combining the frame extract op with a transcode
	// avoid another Google Cloud Storage transfer of the video just to extract a single
	// frame image.
	jobs[0].ExtractVideoFrameOperations = []ExtractVideoFrameOperation{
		NewExtractVideoFrameOperation(source),
	}

	// Before adding job to queue, update expected objects in ProcessingCapture.
	var expectedTranscodeObjects []dstorage.BucketObject
	var expectedVideoFrameObjects []dstorage.BucketObject
	for _, operations := range jobs {
		for _, op := range operations.VideoTranscodeOperations {
			expectedTranscodeObjects = append(expectedTranscodeObjects, op.Destination)
		}
		for _, op := range operations.ExtractVideoFrameOperations {
			expectedVideoFrameObjects = append(expectedVideoFrameObjects, op.Destination)
		}
	}
	pcKey := NewProcessingCaptureKey(eventID, deviceID, contentMD5)
	err = pcKey.PutExpectedTranscodeAndVideoFrameObjects(c, expectedTranscodeObjects, expectedVideoFrameObjects)
	if err != nil {
		log.Errorf(c, "PutExpectedTranscodeAndVideoFrameObjects failed. %v", err)
		return err
	}

	var tasks []*taskqueue.Task
	for _, job := range jobs {
		task, err := job.Task()
		if err != nil {
			log.Errorf(c, "Error getting transcode task. %v", err)
			return err
		}
		tasks = append(tasks, task)
	}

	_, err = taskqueue.AddMulti(c, tasks, "transcode")
	if err != nil {
		log.Errorf(c, "Error adding transcode tasks to queue. %v", err)
		return err
	}

	return nil

}

func postTranscodeCallback(w http.ResponseWriter, r *http.Request, ps httprouter.Params, signedHeaders http.Header) {
	c := appengine.NewContext(r)

	eventID := ps.ByName("event_id")
	deviceID := ps.ByName("device_id")
	contentMD5 := ps.ByName("content_md5")

	transcodeResponse := new(TranscodeOperationsResponse)
	err := json.NewDecoder(r.Body).Decode(transcodeResponse)
	if err != nil {
		log.Errorf(c, "Couldn't decode TranscodeOperationsResponse. %v", err)
		respondWithError(c, w, err)
		return
	}

	pcKey := NewProcessingCaptureKey(eventID, deviceID, contentMD5)
	pc, err := pcKey.AddTranscodesAndFrames(c, transcodeResponse.TranscodedVideos, transcodeResponse.ExtractedFrames)
	if err != nil {
		log.Errorf(c, "AddTranscodesAndFrames failed in transcode callback. %v", err)
		httphelp.RespondInternalServerError(w)
		return
	}

	if pc.ExpectedObjectsExist() {
		log.Debugf(c, "Expected objects all exist. Starting thumbnail process.")
		frame := pc.ExtractedVideoFrames[0]
		err = phase2ScheduleThumbnailJob(c, &frame.Object, eventID, deviceID, contentMD5)
		if err != nil {
			log.Errorf(c, "Error running phase2_scheduleThumbnailJob. %v", err)
			respondWithError(c, w, err)
			return
		}
	} else {
		log.Debugf(c, "Still waiting for expected object.")
	}

	w.WriteHeader(http.StatusNoContent)
}

func phase2ScheduleThumbnailJob(c context.Context, source *dstorage.BucketObject, eventID, deviceID, contentMD5 string) error {

	hostname, err := CurrentModuleVersionHostname(c)
	if err != nil {
		log.Errorf(c, "Couldn't get module hostname. %v", err)
		return err
	}

	// /events/:event_id/captures/:device_id/:content_md5/capture
	callbackURL := fmt.Sprintf("https://%v/events/%v/captures/%v/%v/thumbnailcallback", hostname, eventID, deviceID, contentMD5)

	callback := signedrequest.SignedRequest{
		Method:     "POST",
		URL:        callbackURL,
		Expiration: time.Now().Add(24 * time.Hour),
	}
	err = callback.Sign(c)
	if err != nil {
		log.Errorf(c, "Failed to sign url. %v", err)
		return err
	}

	ops := &ThumbnailOperationsRequest{
		Source: *source,
		ThumbnailOperations: []*ThumbnailOperation{
			NewResizeOperation(source, 32),
			NewResizeOperation(source, 64),
			NewResizeOperation(source, 128),
			NewResizeOperation(source, 256),
			NewResizeOperation(source, 512),
			NewResizeOperation(source, 1024),
			NewResizeOperation(source, 2048),
		},
		Callback: callback,
	}

	err = ops.QueueJob(c)
	if err != nil {
		log.Errorf(c, "Error adding task to thumbnail queue. %v", err)
		return err
	}

	return nil
}

var delayNotifyEventSubscribers = delay.Func("NotifyEventSubscribers", NotifyEventSubscribers)
var delayDeleteProcessingCapture = delay.Func("DeleteProcessingCapture", DeleteProcessingCapture)

func postThumbnailCallback(w http.ResponseWriter, r *http.Request, ps httprouter.Params, signedHeaders http.Header) {
	eventID := ps.ByName("event_id")
	deviceID := ps.ByName("device_id")
	contentMD5 := ps.ByName("content_md5")

	c := appengine.NewContext(r)

	thumbnails := new(ThumbnailOperationsResponse)
	err := json.NewDecoder(r.Body).Decode(thumbnails)
	if err != nil {
		log.Errorf(c, "Error decoding thumbnail response JSON. %v", err)
		httphelp.RespondBadRequest(w, "Invalid JSON. "+err.Error())
		return
	}

	// Check the device is allowed to post to this event. This is a signed request,
	// and whoever generates it is expected to have authenticated the device already.
	event, err := GetEventByID(c, eventID)
	if err != nil {
		httphelp.RespondNotFound(w)
		return
	}

	if !event.CanPost(deviceID) {
		httphelp.RespondForbidden(w)
		return
	}

	// Get the ProcessingCapture
	pcKey := NewProcessingCaptureKey(eventID, deviceID, contentMD5)
	pc, err := pcKey.GetProcessingCapture(c)
	var capture *Capture
	if err == nil {
		capture = &pc.Capture
	} else if err == datastore.ErrNoSuchEntity {
		capture, err = NewCapture(c, eventID, deviceID, contentMD5)
		if err != nil {
			log.Errorf(c, "NewCapture returned error in thumbnail callback. %v", err)
			respondWithError(c, w, err)
			return
		}
	} else {
		log.Errorf(c, "GetProcessingCapture returned unexpected error in thumbnail callback. %v", err)
		respondWithError(c, w, err)
		return
	}

	// Get the ContentType stored in Google Cloud Storage and cache it with the Capture object.
	objectName := storageObjectName(eventID, deviceID, contentMD5)
	contentType, err := fetchContentType(c, DefaultStorageBucket.Name, objectName)
	if err != nil {
		log.Errorf(c, "Error fetching content type for %v", objectName)
		httphelp.RespondInternalServerError(w)
		return
	}

	// Write the Capture
	capture.Thumbnails = thumbnails.Thumbnails
	capture.ContentType = contentType

	err = capture.Put(c)
	if err != nil {
		respondWithError(c, w, err)
		return
	}

	err = delayNotifyEventSubscribers.Call(c, eventID, deviceID)
	if err != nil {
		log.Errorf(c, "Delay failed notifying subscribers. %v", err)
	}

	err = delayDeleteProcessingCapture.Call(c, pcKey)
	if err != nil {
		log.Errorf(c, "Delay failed cleaning up processing capture. %v", err)
	}

	w.WriteHeader(http.StatusNoContent)
}

func deleteCapture(w http.ResponseWriter, r *http.Request, ps httprouter.Params, context AuthenticatedContext) {
	eventID := ps.ByName("event_id")
	deviceID := ps.ByName("device_id")
	contentMD5 := ps.ByName("content_md5")

	if eventID == "" || deviceID == "" || contentMD5 == "" {
		httphelp.RespondBadRequest(w, "Invalid event, device, or content md5")
		return
	}

	c := appengine.NewContext(r)

	if context.AuthenticatedDeviceID() != deviceID {
		// User isn't the owner of the content. User only allowed to delete someone else's
		// content if they own the event.
		event, err := GetEventByID(c, eventID)
		if err != nil {
			log.Debugf(c, "GetEventByID failed. %v", err.Error())
			httphelp.RespondBadRequest(w, "Invalid event")
			return
		}
		if event.Owner != context.AuthenticatedDeviceID() {
			httphelp.RespondForbidden(w)
			return
		}
	}

	// Remove the capture from the event, marking it for deletion. Another process will sweep through
	// and delete everything.
	err := DeleteCapture(c, eventID, deviceID, contentMD5)
	if err != nil {
		if err == datastore.ErrNoSuchEntity {
			httphelp.RespondNotFound(w)
			return
		} else {
			log.Errorf(c, "Error deleting capture. %v", err)
			respondWithError(c, w, err)
			return
		}
	}

	w.WriteHeader(http.StatusOK)
}

func storageObjectName(eventID, deviceID, contentMD5 string) string {
	return deviceID + "/" + eventID + "/" + contentMD5
}

func getData(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	eventID := ps.ByName("event_id")
	deviceID := ps.ByName("device_id")
	contentMD5 := ps.ByName("content_md5")

	if eventID == "" || deviceID == "" || contentMD5 == "" {
		httphelp.RespondBadRequest(w, "Invalid event, device, or content md5")
		return
	}

	url := DefaultStorageBucket.PublicGetURL(storageObjectName(eventID, deviceID, contentMD5))

	// Since there is a one to one relation from device_id/asset_id to Public Get URL,
	// we can respond with 301 Moved Permanently to allow the client to cache this response.
	httphelp.RespondMovedPermanently(w, url)
}

func getDataUploadURL(w http.ResponseWriter, r *http.Request, ps httprouter.Params, authContext AuthenticatedContext) {
	eventID := ps.ByName("event_id")
	deviceID := ps.ByName("device_id")
	contentMD5 := ps.ByName("content_md5")
	contentType := r.URL.Query().Get("contenttype")
	size := r.URL.Query().Get("size")

	if contentType == "" {
		httphelp.RespondBadRequest(w, "Missing contenttype query parameter")
		return
	}

	// If the client provides a size, preflight it for them to make sure its okay.
	// This does nothing to enforce the client is going to upload something of this size. This is simply
	// so that the client can fail quickly instead of going through the entire upload. If, however, a client
	// upload something > MaxUserFileUploadSize, then it will be deleted through the GCS Object Change Notification handler.
	if size != "" {
		sizeInt, err := strconv.Atoi(size)
		if err != nil {
			httphelp.RespondBadRequest(w, "Error parsing size parameter. "+err.Error())
			return
		}

		if sizeInt < 0 || sizeInt > MaxUserFileUploadSize {
			msg := fmt.Sprintf("Size (%v) out of range. Should be between 0 and %v.", sizeInt, MaxUserFileUploadSize)
			httphelp.RespondBadRequest(w, msg)
			return
		}
	}

	if eventID == "" || deviceID == "" || contentMD5 == "" {
		httphelp.RespondBadRequest(w, "event id, device id, or content md5 is empty")
		return
	}

	if authContext.AuthenticatedDeviceID() != deviceID {
		httphelp.RespondForbidden(w)
		return
	}

	c := appengine.NewContext(r)

	// Check the device is allowed to post to this event.
	event, err := GetEventByID(c, eventID)
	if err != nil {
		httphelp.RespondNotFound(w)
		return
	}

	if !event.CanPost(deviceID) {
		httphelp.RespondForbidden(w)
		return
	}

	name := storageObjectName(eventID, deviceID, contentMD5)

	expiry := 7 * 24 * time.Hour
	signedURL, err := DefaultStorageBucket.SignedPutURL(c, name, contentType, contentMD5, expiry)

	if err != nil {
		respondWithError(c, w, err)
		return
	}

	v := map[string]interface{}{"url": signedURL}
	httphelp.RespondJSON(w, r, v)
}

func putDevice(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	deviceID := ps.ByName("device_id")

	decoder := json.NewDecoder(r.Body)
	device := new(Device)
	err := decoder.Decode(device)
	if err != nil {
		httphelp.RespondBadRequest(w, "Invalid JSON: "+err.Error())
		return
	}
	device.ID = deviceID

	// Write to datastore
	c := appengine.NewContext(r)
	err = device.Put(c)
	if err != nil {
		respondWithError(c, w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// MovieMakerJSON is the client JSON to specify a movie.
type MovieMakerJSON struct {
	Captures []CaptureJSON `json:"captures"`
}

func postMakeMovie(w http.ResponseWriter, r *http.Request, ps httprouter.Params, authContext AuthenticatedContext) {
	eventID := ps.ByName("event_id")

	c := appengine.NewContext(r)

	decoder := json.NewDecoder(r.Body)
	var spec MovieMakerJSON
	err := decoder.Decode(&spec)
	if err != nil {
		httphelp.RespondBadRequest(w, "Invalid JSON: "+err.Error())
		return
	}

	hostname, err := CurrentModuleVersionHostname(c)
	if err != nil {
		log.Errorf(c, "Couldn't get module hostname. %v", err)
		httphelp.RespondInternalServerError(w)
		return
	}
	// /events/:event_id/devices/:device_id/makemoviecallback
	callbackURL := fmt.Sprintf("https://%v/events/%v/devices/%v/makemoviecallback", hostname, eventID, authContext.AuthenticatedDeviceID())
	log.Debugf(c, "Using callback URL: %v", callbackURL)
	callback := signedrequest.SignedRequest{
		Method:     "POST",
		URL:        callbackURL,
		Expiration: time.Now().Add(7 * 24 * time.Hour),
	}
	err = callback.Sign(c)
	if err != nil {
		log.Errorf(c, "Couldn't sign request. %v", err)
		httphelp.RespondInternalServerError(w)
		return
	}

	assets := []dstorage.BucketObject{}
	for _, capture := range spec.Captures {
		objectName := storageObjectName(eventID, capture.Owner, capture.ContentMD5)
		assets = append(assets, dstorage.BucketObject{Bucket: DefaultStorageBucket.Name, Object: objectName})
	}

	maker := &MovieMaker{
		Assets:           assets,
		OutputBucket:     DefaultStorageBucket.Name,
		ObjectNamePrefix: storageObjectName(eventID, authContext.AuthenticatedDeviceID(), ""),
		Callback:         callback,
	}

	err = maker.QueueJob(c)
	if err != nil {
		log.Errorf(c, "QueueJob failed. %v", err)
		httphelp.RespondInternalServerError(w)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func postMakeMovieCallback(w http.ResponseWriter, r *http.Request, ps httprouter.Params, signedHeaders http.Header) {

	eventID := ps.ByName("event_id")
	deviceID := ps.ByName("device_id")

	if eventID == "" || deviceID == "" {
		httphelp.RespondBadRequest(w, "Missing event_id or device_id")
		return
	}

	var BucketObject dstorage.BucketObject
	err := json.NewDecoder(r.Body).Decode(&BucketObject)
	if err != nil {
		httphelp.RespondBadRequest(w, "Invalid json: "+err.Error())
		return
	}

	c := appengine.NewContext(r)
	log.Debugf(c, "Post make movie callback. Bucket: %v, Object: %v", BucketObject.Bucket, BucketObject.Object)

	w.WriteHeader(http.StatusNoContent)
}

// Google Cloud Storage Object Change Notification handler as described by
// https://cloud.google.com/storage/docs/object-change-notification#_Notification_Types
func postGCSObjectChangeNotification(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	c := appengine.NewContext(r)

	channelId := r.Header.Get("X-Goog-Channel-Id")
	if channelId != "limitcheck" {
		log.Infof(c, "Invalid channel id %v. Should be limitcheck.", channelId)
		httphelp.RespondBadRequest(w, "Invalid channel id "+channelId)
		return
	}

	// The client token is set in the gsutil command that starts the object change notification.
	// This token was generated with uuidgen.
	// Start the watch by running:
	// gsutil notification watchbucket -t "F4B0F3C1-0A9A-4D5E-8EDB-228FA1F68C7D" https://stitch-1026.appspot.com/gcs_object_change_notification gs://stitch
	channelToken := r.Header.Get("X-Goog-Channel-Token")
	if channelToken != "F4B0F3C1-0A9A-4D5E-8EDB-228FA1F68C7D" {
		log.Infof(c, "Invalid channel token %v.", channelToken)
		httphelp.RespondForbidden(w)
		return
	}

	resourceState := r.Header.Get("X-Goog-Resource-State")
	switch resourceState {
	case "sync":
		// initial sync message
		log.Infof(c, "Got sync request")
	case "not_exists":
		// object deleted
		break
	case "exists":
		o := storage.Object{}
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&o)
		if err != nil {
			log.Errorf(c, "Failed to parse JSON. %v", err)
			httphelp.RespondBadRequest(w, "Couldn't parse JSON.")
			return
		}

		if o.Size > MaxUserFileUploadSize {
			log.Warningf(c, "Deleting object whose file size %v exceeds limit %v. %v", o.Size, MaxUserFileUploadSize, o.Name)

			client := googleapiclient.NewClient(c, storage.DevstorageFullControlScope)
			storageService, err := storage.New(client)

			if err != nil {
				log.Errorf(c, "Couldn't get storage client. %v", err)
				httphelp.RespondInternalServerError(w)
				return
			} else {
				err := storageService.Objects.Delete(o.Bucket, o.Name).Do()
				if err != nil {
					log.Errorf(c, "Error deleting object %v. %v", o.Name, err)

					sa, err := appengine.ServiceAccount(c)
					log.Debugf(c, "Current service account is %v. err=%v", sa, err)

					httphelp.RespondInternalServerError(w)
					return
				}
			}
		}
	}

	w.WriteHeader(http.StatusNoContent)
}

func postPairingCode(w http.ResponseWriter, r *http.Request, ps httprouter.Params, context AuthenticatedContext) {
	c := appengine.NewContext(r)

	const expiration = 1 * time.Hour
	pair, err := NewUniquePairingCode(c, context.AuthenticatedDeviceID(), expiration)
	if err != nil {
		log.Errorf(c, "Error creating pair code. %v", err)
		httphelp.RespondInternalServerError(w)
		return
	}

	w.Header().Set("Location", "/pair/codes/"+pair.Code)
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(pair.Code))
}

func getPairingCodeValue(w http.ResponseWriter, r *http.Request, ps httprouter.Params, context AuthenticatedContext) {
	c := appengine.NewContext(r)
	code := ps.ByName("code")
	if code == "" {
		httphelp.RespondBadRequest(w, "Missing code")
		return
	}

	pairingCode, err := GetPairingCode(c, code)
	if err != nil {
		respondWithError(c, w, err)
		return
	}

	if pairingCode.Owner != context.AuthenticatedDeviceID() {
		httphelp.RespondForbidden(w)
		return
	}

	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(pairingCode.Value))
}

func putPairingCodeValue(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	c := appengine.NewContext(r)
	code := ps.ByName("code")
	if code == "" {
		httphelp.RespondBadRequest(w, "Missing code")
		return
	}

	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		httphelp.RespondInternalServerError(w)
		return
	}
	value := string(b)

	err = PutPairingCodeValue(c, code, value)
	if err != nil {
		respondWithError(c, w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func getCleanupExpiredCodes(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	c := appengine.NewContext(r)
	log.Debugf(c, "cron job called for expired codes")

	err := DeleteExpiredCodes(c)
	if err != nil {
		log.Errorf(c, "Error cleaning up expired codes. %v", err)
		httphelp.RespondInternalServerError(w)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func respondWithError(c context.Context, w http.ResponseWriter, err error) {
	var status int
	if err == ErrForbidden {
		status = http.StatusForbidden
	} else if err == ErrInvalidInput {
		status = http.StatusBadRequest
	} else if err == datastore.ErrNoSuchEntity || err == ErrNotFound {
		status = http.StatusNotFound
	} else if googleErr, ok := err.(*googleapi.Error); ok {
		status = googleErr.Code
		log.Debugf(c, "Google API Error. Error: %v", err)
	} else {
		status = http.StatusInternalServerError
		var buf = make([]byte, 10000)
		runtime.Stack(buf, false)
		log.Errorf(c, "Internal Server Error. Error: %v, Call Stack:\n", err, string(buf))
	}

	w.WriteHeader(status)
}

type StitchHandler struct {
	router *httprouter.Router
}

func (handler *StitchHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Access-Control-Allow-Origin", "*")
	if r.Method == "OPTIONS" {
		w.Header().Set("Access-Control-Max-Age", strconv.Itoa(int((24 * time.Hour).Seconds())))
		if v, ok := r.Header["Access-Control-Request-Method"]; ok {
			w.Header()["Access-Control-Allow-Methods"] = v
		}
		if v, ok := r.Header["Access-Control-Request-Headers"]; ok {
			w.Header()["Access-Control-Allow-Headers"] = v
		}
		w.WriteHeader(http.StatusNoContent)
		return
	} else {
		handler.router.ServeHTTP(w, r)
	}
}

// RestAPIHandlers returns an http.Handler that can implement the stitch service.
func RestAPIHandlers() http.Handler {
	router := httprouter.New()

	// Devices
	router.PUT("/devices/:device_id", putDevice)

	// Events
	router.GET("/events", getEvents)
	router.GET("/events/:event_id", getEvent)
	router.PUT("/events/:event_id", authenticateDeviceHandler(putEvent))
	router.PUT("/events/:event_id/location", authenticateDeviceHandler(putEventLocation))
	router.PUT("/events/:event_id/subscribers/:device_id", authenticateDeviceHandler(putEventSubscriber))
	router.DELETE("/events/:event_id/subscribers/:device_id", authenticateDeviceHandler(deleteEventSubscriber))

	// Captures
	router.GET("/events/:event_id/captures/:device_id/:content_md5/data", getData)
	router.GET("/events/:event_id/captures/:device_id/:content_md5/datauploadurl", authenticateDeviceHandler(getDataUploadURL))
	router.PUT("/events/:event_id/captures/:device_id/:content_md5/uploadcomplete", authenticateDeviceHandler(putUploadComplete))
	router.POST("/events/:event_id/captures/:device_id/:content_md5/thumbnailcallback", signedRequestHandler(postThumbnailCallback))
	router.POST("/events/:event_id/captures/:device_id/:content_md5/transcodecallback", signedRequestHandler(postTranscodeCallback))
	router.DELETE("/events/:event_id/captures/:device_id/:content_md5", authenticateDeviceHandler(deleteCapture))

	// Movies
	router.POST("/events/:event_id/makemovie", authenticateDeviceHandler(postMakeMovie))
	router.POST("/events/:event_id/devices/:device_id/makemoviecallback", signedRequestHandler(postMakeMovieCallback))

	// Google Cloud Storage Object Change Notification Handler
	router.POST("/gcs_object_change_notification", postGCSObjectChangeNotification)

	// Pairing with TVs
	router.POST("/pair/codes", authenticateDeviceHandler(postPairingCode))
	router.GET("/pair/codes/:code/value", authenticateDeviceHandler(getPairingCodeValue))
	router.PUT("/pair/codes/:code/value", putPairingCodeValue)
	router.GET("/pair/cleanup", cronHandler(getCleanupExpiredCodes))

	return &StitchHandler{router: router}
}
