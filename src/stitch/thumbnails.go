package stitch

import (
	"encoding/json"
	"fmt"
	"github.com/drichardson/appengine/signedrequest"
	"github.com/drichardson/appengine/storage"
	"golang.org/x/net/context"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/taskqueue"
)

// Thumbnail contains some metadata and the location of a thumbnail.
type Thumbnail struct {
	Width  int                  `json:"width"`
	Height int                  `json:"height"`
	Object storage.BucketObject `json:"object"`
}

// ThumbnailOperationsResponse contains the thumbnails generated from a successful
// Thumbnail
type ThumbnailOperationsResponse struct {
	Thumbnails []Thumbnail `json:"thumbnails"`
}

// ThumbnailOperation defines a thumbnail operation to perform and where to put the result.
type ThumbnailOperation struct {
	Destination storage.BucketObject   `json:"destination"`
	Op          string                 `json:"op"`
	Parameters  map[string]interface{} `json:"parameters"`
}

// NewResizeOperation creates an Operation that will resize an image so that no edge
// is greater than maxEdge. It places the result next to the source file in
// Google Cloud Storage, appending a suffix of lte_PIXELS_pixels, where
// lte means "less than or equal".
func NewResizeOperation(source *storage.BucketObject, maxEdge int) *ThumbnailOperation {
	return &ThumbnailOperation{
		Destination: storage.BucketObject{
			Bucket: source.Bucket,
			Object: source.Object + fmt.Sprintf(".lte_%d_pixels", maxEdge*maxEdge),
		},
		Op: "resize",
		Parameters: map[string]interface{}{
			"width":  maxEdge,
			"height": maxEdge,
		},
	}
}

// ThumbnailOperationsRequest is used to request thumbnail operations from the
// thumbnail service.
type ThumbnailOperationsRequest struct {
	Source              storage.BucketObject        `json:"source"`
	ThumbnailOperations []*ThumbnailOperation       `json:"operations"`
	Callback            signedrequest.SignedRequest `json:"callback"`
}

// QueueJob add a ThumbnailOperationsRequest to the thumbnail taskqueue.
func (ops *ThumbnailOperationsRequest) QueueJob(c context.Context) error {
	opsBytes, err := json.Marshal(ops)
	if err != nil {
		log.Errorf(c, "Failed to marshall ops. %v", err)
		return err
	}

	task := &taskqueue.Task{
		Method:  "PULL",
		Payload: opsBytes,
	}

	_, err = taskqueue.Add(c, task, "thumbnail")
	if err != nil {
		log.Errorf(c, "Error adding task to thumbnail queue. %v", err)
		return err
	}

	return nil
}
