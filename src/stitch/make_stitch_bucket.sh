#!/bin/bash
# Create the stitch bucket
set -e
BUCKET="gs://stitch"

# Create the bucket
gsutil mb $BUCKET

# Give everyone read permission on bucket objects by default.
gsutil defacl ch -u AllUsers:R $BUCKET

# Setup cross origin resource sharing
./set_cors "$BUCKET"
