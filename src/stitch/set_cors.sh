#!/bin/bash

BUCKET=$1
shift
if [ -z "$BUCKET" ]; then
    echo "Missing bucket"
    exit 1
fi

gsutil cors set cors.json "$BUCKET"
