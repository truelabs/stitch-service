// +build appengine

package stitch

import (
	"net/http"
)

func init() {
	http.Handle("/", RestAPIHandlers())
}
