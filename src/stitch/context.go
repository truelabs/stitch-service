package stitch

type authenticatedContext struct {
	deviceID string
}

// AuthenticatedContext identifies a particular device. It has the same restrictions
// as Context.
type AuthenticatedContext interface {
	AuthenticatedDeviceID() string
}

// NewAuthenticatedDeviceContext get an AuthenticationContext from the device ID.
// This method does not authenticate the device. It's expected the caller will do that.
func NewAuthenticatedDeviceContext(authenticatedDeviceID string) AuthenticatedContext {
	return &authenticatedContext{deviceID: authenticatedDeviceID}
}

// AuthenticatedDeviceID gets the device ID for the context.
func (c *authenticatedContext) AuthenticatedDeviceID() string {
	return c.deviceID
}
