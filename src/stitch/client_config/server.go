package main

import (
	"log"
	"net/http"
)

func main() {

	log.SetFlags(log.Lmicroseconds | log.Ltime)
	log.Println("Started")

	http.HandleFunc("/client_config.json", func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.Method, "REQUEST", "User-Agent", r.UserAgent(), "Headers:", r.Header)
		w.Header().Set("Cache-Control", "max-age=10")
		http.ServeFile(w, r, "client_config.json")
	})

	err := http.ListenAndServe(":8000", nil)
	log.Fatal(err)
}
