#!/bin/bash
# Create the stitch bucket
set -e
BUCKET="gs://stitch-client-config"

# Create the bucket
gsutil mb $BUCKET

# Give everyone read permission on bucket objects by default.
gsutil defacl ch -u AllUsers:R $BUCKET
