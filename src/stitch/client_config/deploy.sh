#!/bin/bash

# MAX_AGE is the HTTP Cache-Control directive that specifies in number of seconds
# clients ought to cache the response.
MAX_AGE_PROD=86400
MAX_AGE_DEV=10

usage() {
    echo "deploy.sh <source_file> <prod|dev>"
}

validateJSON() {
    echo "Validating JSON..."
    which jsonlint
    if [ "$?" != 0 ]; then
        echo "Couldn't find jsonlint which is used to validate syntax of client config."
        echo "Install it using: npm install jsonlint -g"
        echo "If you don't have npm, install it using brew install npm"
        exit 1
    fi
    jsonlint $1 || exit 1
    echo "JSON is valid."
}

fileSize() {
    stat -f "%z" "$1"
}

SOURCE=$1
shift

if [ -z "$SOURCE" ]; then
    echo "Missing source_file"
    usage
    exit 1
fi

case $1 in
    "prod")
        DESTINATION="client_config.json"
        MAX_AGE=$MAX_AGE_PROD
        ;;
    "dev")
        DESTINATION="dev_client_config.json"
        MAX_AGE=$MAX_AGE_DEV
        ;;
    *)
        echo "Invalid target '$TARGET'"
        usage
        exit 1
esac
shift

validateJSON $SOURCE

set -e

gsutil -h "Content-Type:application/json" -h "Cache-Control:public, max-age=$MAX_AGE" cp -z json "$SOURCE" "gs://stitch-client-config/$DESTINATION"
echo "OK"

echo "To view file using curl, use:"
echo "  curl --compressed https://storage.googleapis.com/stitch-client-config/$DESTINATION"
