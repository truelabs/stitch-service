Client Config
=============
Client config contains parameters that can be adjusted without requiring new versions of the aps
to be submitted to the app stores.

deploy.sh
=========
Deploy a client config to either prod or dev environments. Does a basic JSON syntax check before uploading.
Uploads the JSON to S3, using Cache-Control policies defined in the script.

server.go
===========
The test server is just a simple server that can be used to log requests and modify headers, especially
Cache-Control to verify clients aren't over or under requesting the client config. This is nice because
the actual client configs are deployed on S3, which doesn't give (afaik) up to date logs.

To run: `go run server.go`

