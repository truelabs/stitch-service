package stitch

import (
	"errors"
)

// Error code for unauthorized access.
var ErrForbidden = errors.New("ErrForbidden")

// Error code for no object.
var ErrNotFound = errors.New("ErrNotFound")

// Error code for incomplete datastore key.
var ErrIncompleteKey = errors.New("ErrIncompleteKey")

// Error code for invalid input.
var ErrInvalidInput = errors.New("ErrInvalidInput")
