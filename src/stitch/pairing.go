package stitch

import (
	"github.com/drichardson/codes"
	"golang.org/x/net/context"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"time"
)

// PairingCode associates values with codes. The codes are designed to be
// easy for a user to enter. The initial use case of pairing codes is to join
// an event from a Apple TV.
type PairingCode struct {
	Owner      string
	Code       string
	Value      string
	Expiration time.Time
}

const pairingCodeKind = "PairingCode"

func keyFromCode(c context.Context, code string) *datastore.Key {
	return datastore.NewKey(c, pairingCodeKind, code, 0, nil)
}

// NewUniquePairingCode returns a new PairingCode with a randomly generated
// code on success.
func NewUniquePairingCode(c context.Context, owner string, expiration time.Duration) (*PairingCode, error) {
	pairingCode := &PairingCode{}
	const maxCodesToTry = 10
	err := datastore.RunInTransaction(c, func(c context.Context) error {
		for i := 0; i < maxCodesToTry; i++ {
			code, err := codes.NewRandomDecimalString(8)
			if err != nil {
				// shouldn't happen, just give up if it does
				return err
			}
			key := keyFromCode(c, code)
			err = datastore.Get(c, key, pairingCode)
			if err == datastore.ErrNoSuchEntity {
				// great, let use this one
				pairingCode.Code = code
				pairingCode.Expiration = time.Now().Add(expiration)
				pairingCode.Owner = owner
				_, err := datastore.Put(c, key, pairingCode)
				return err
			} else if err != nil {
				// got some unexpected error. bail out now
				return err
			} else {
				// code in use, try another one
			}
		}
		return ErrNotFound
	}, nil)

	if err != nil {
		return nil, err
	}

	return pairingCode, nil
}

// PutPairingCodeValue a value with a code.
func PutPairingCodeValue(c context.Context, code string, value string) error {
	return datastore.RunInTransaction(c, func(c context.Context) error {
		key := keyFromCode(c, code)
		pairingCode := &PairingCode{}
		err := datastore.Get(c, key, pairingCode)
		if err != nil {
			return err
		}
		pairingCode.Value = value
		_, err = datastore.Put(c, key, pairingCode)
		return err
	}, nil)
}

// GetPairingCode returns the PairingCode with the given code.
func GetPairingCode(c context.Context, code string) (*PairingCode, error) {
	pairingCode := &PairingCode{}
	err := datastore.Get(c, keyFromCode(c, code), pairingCode)
	if err != nil {
		return nil, err
	}
	return pairingCode, nil
}

func DeleteExpiredCodes(c context.Context) error {
	keys, err := datastore.NewQuery(pairingCodeKind).Filter("Expiration <", time.Now()).KeysOnly().Limit(1000).GetAll(c, nil)
	if err != nil {
		log.Errorf(c, "Expired code query failed. %v", err)
		return err
	}

	err = datastore.DeleteMulti(c, keys)
	if err != nil {
		log.Errorf(c, "Error deleting %v keys. %v", len(keys), err)
		return err
	}

	return err
}
