package stitch

// Sanity check limits on client provided inputs

// Client IDs are expected to be generated using UUIDs, so typically
// 36 characters is enough for the typical representation (e.g.,
// de305d54-75b4-431b-adb2-eb6b9e546014). However, since clients
// may want to SHA256 something unique to them, we will set the
// limit to 64.
const MaxClientIDLen = 64

// Max passwords are set to 64 as the client should only be communicating
// randomly generated passwords or salted SHA256 of the user's password.
const (
	MaxClientPasswordLen = 64
	MinClientPasswordLen = 8
)

const MaxUserFileUploadSize = 500 * 1000000
