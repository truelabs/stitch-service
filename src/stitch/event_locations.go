package stitch

import (
	"encoding/base64"
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/search"
	"time"
)

// EventLocation is a geographical location at a moment in time.
type EventLocation struct {
	Location appengine.GeoPoint
	Updated  time.Time
}

const eventLocationIndexName = "EventLocation"

// PutEventLocation writes an location to a geo-spatial index.
func PutEventLocation(c context.Context, eventID string, geopoint appengine.GeoPoint) error {

	if eventID == "" {
		return ErrInvalidInput
	}

	if !geopoint.Valid() {
		return ErrInvalidInput
	}

	// Make event ID safe for search service. From the documentation:
	//     https://cloud.google.com/appengine/docs/go/search/
	//     A docID must contain only visible, printable ASCII characters
	//     (ASCII codes 33 through 126 inclusive) and be no longer than 500
	//     characters. A document identifier cannot begin with an
	//     exclamationpoint ('!'), and it can't begin and end with double
	//     underscores ("__").
	//
	// A standard alphabet base64 encoding meets the requirements for a docID.
	docID := base64.StdEncoding.EncodeToString([]byte(eventID))

	index, err := search.Open(eventLocationIndexName)
	if err != nil {
		return err
	}

	eventLocation := &EventLocation{Location: geopoint, Updated: time.Now()}
	_, err = index.Put(c, docID, eventLocation)
	return err
}

// GetMostRecentEventsIDsNear returns a list of events that are near geopoint.
func GetMostRecentEventsIDsNear(c context.Context, geopoint appengine.GeoPoint, maxMetersFromPoint float64) ([]string, error) {
	if !geopoint.Valid() {
		return []string{}, ErrInvalidInput
	}

	index, err := search.Open(eventLocationIndexName)
	if err != nil {
		return []string{}, err
	}

	distance := fmt.Sprintf("distance(Location, geopoint(%f,%f))", geopoint.Lat, geopoint.Lng)
	const halfCircumferenceEarthMeters = 40075000.0 / 2.0 // doesn't really have to be this, just something bigger than any real distance.
	sortExpr := []search.SortExpression{search.SortExpression{Expr: distance, Default: halfCircumferenceEarthMeters, Reverse: true}}
	sortOpts := &search.SortOptions{Expressions: sortExpr}
	opts := &search.SearchOptions{IDsOnly: true, Sort: sortOpts}
	query := fmt.Sprintf("%v < %f", distance, maxMetersFromPoint)
	log.Debugf(c, "event locationn query: %v", query)

	var eventIDs []string

	for iterator := index.Search(c, query, opts); ; {
		id, err := iterator.Next(nil)
		if err == search.Done {
			break
		}
		if err != nil {
			return []string{}, err
		}
		decodedID, err := base64.StdEncoding.DecodeString(id)
		if err != nil {
			log.Debugf(c, "Skipping result: Error base64 decoded id into event ID. id=%v. err=%v", id, err)
			continue
		}
		eventIDs = append(eventIDs, string(decodedID))
	}

	return eventIDs, nil
}
