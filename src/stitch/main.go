// -build appengine

package stitch

import (
	"bitbucket.org/truelabs/httphelp"
	"flag"
	"log"
	"net/http"
)

var listenAddress = flag.String("addr", ":8080", "Listens on the TCP network address")

func main() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
	flag.Parse()
	err := http.ListenAndServe(*listenAddress, httphelp.RequestLoggerHandler(RestAPIHandlers()))
	log.Fatalln("ListenAndServe failed.", err)
}
