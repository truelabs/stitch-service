package stitch

import (
	"golang.org/x/net/context"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/memcache"
	"time"
)

// Event defines an event parameters, but not the content.
// Note: by default, new Event fields are immutably stored in the datatore. That is, new Puts only
// update a set of whitelisted fields. Modify Event.Put if you want other fields to be updated.
type Event struct {
	ID            string         `json:"id" datastore:"-"`
	Name          string         `json:"name"`
	Owner         string         `json:"owner" datastore:"-"`
	OwnerKey      *datastore.Key `json:"-"`
	Created       time.Time      `json:"created"`
	AnyoneCanPost bool           `json:"anyone_can_post"`
}

// EventKeyForID returns the datastore key for an event ID.
func EventKeyForID(c context.Context, id string) (*datastore.Key, error) {
	key := datastore.NewKey(c, "Event", id, 0, nil)
	if key.Incomplete() {
		return nil, ErrIncompleteKey
	}
	return key, nil
}

func getEventFromMemcache(c context.Context, id string) (*Event, error) {
	event := new(Event)
	if _, err := memcache.JSON.Get(c, id, event); err == nil {
		// Restore non-JSON fields
		ownerKey, err := DeviceKeyForID(c, event.Owner)
		if err != nil {
			return nil, err
		}
		event.OwnerKey = ownerKey
		return event, nil
	} else {
		return nil, err
	}
}

// GetEventByID returns datastore.ErrNoSuchEntity if no item exists.
func GetEventByID(c context.Context, id string) (*Event, error) {
	if event, err := getEventFromMemcache(c, id); err == nil {
		return event, nil
	} else if err != memcache.ErrCacheMiss {
		log.Errorf(c, "memcache fail: event: %v", err)
	}

	key, err := EventKeyForID(c, id)
	if err != nil {
		return nil, err
	}
	event := new(Event)
	err = datastore.Get(c, key, event)
	if err != nil {
		return nil, err
	}
	event.ID = key.StringID()
	event.Owner = event.OwnerKey.StringID()

	if err := memcache.JSON.Set(c, &memcache.Item{Key: id, Object: event}); err != nil {
		log.Errorf(c, "Failed to write event to memcache. %v", err)
	}

	return event, nil
}

// Put an event to the datastore.
// Returns ErrForbidden if there is an existing event and the
// owner doesn't match. Note this method does not authenticate the
// owner is who they say they are. Use AuthenticatedContext to do that
// before calling this method.
func (e *Event) Put(c context.Context) error {
	// Convert id to key. Fails if id isn't set.
	key, err := EventKeyForID(c, e.ID)
	if err != nil {
		return err
	}

	// Convert Owner into an OwnerKey. Fails if owner isn't set.
	e.OwnerKey, err = DeviceKeyForID(c, e.Owner)
	if err != nil {
		return err
	}

	// Make sure required fields are set.
	if e.Name == "" {
		return ErrInvalidInput
	}

	result := datastore.RunInTransaction(c, func(c context.Context) error {
		existingEvent, err := GetEventByID(c, e.ID)
		if err == datastore.ErrNoSuchEntity {
			// no existing entry, just write the new one.
			e.Created = time.Now()
			_, err := datastore.Put(c, key, e)

			return err
		} else if err == nil {
			// there is an existing entry. Make sure the owner is the same.
			if existingEvent.Owner != e.Owner {
				return ErrForbidden
			}
			// Fields are immutable, except for the ones below
			existingEvent.Name = e.Name
			existingEvent.AnyoneCanPost = e.AnyoneCanPost
			_, err := datastore.Put(c, key, existingEvent)
			return err
		} else {
			return err
		}
	}, nil)

	if result == nil {
		if err := memcache.JSON.Set(c, &memcache.Item{Key: e.ID, Object: e}); err != nil {
			log.Errorf(c, "Error setting memcache after writing event to datastore. %v", err)
		}
	}

	return result
}

// CanPost determines if this userID is authorized to post to this Event.
func (e *Event) CanPost(userID string) bool {
	return e.AnyoneCanPost || userID == e.Owner
}
