package stitch

import (
	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
	"strings"
)

// CurrentModuleVersionHostname builds an HTTP URL suitable for sending over to an instance
// of the same module and version as the currently running server.
// For more information, see: https://cloud.google.com/appengine/docs/go/modules/routing#routing_via_url
func CurrentModuleVersionHostname(c context.Context) (string, error) {
	module := appengine.ModuleName(c)
	versionXY := appengine.VersionID(c)
	appID := appengine.AppID(c)

	versionComponents := strings.Split(versionXY, ".")
	if len(versionComponents) != 2 {
		log.Errorf(c, "Expected version to have 2 components but got %v", len(versionComponents))
		return "", ErrInvalidInput
	}

	// hostname for use in a URL like https://version-dot-module-dot-app-id.appspot.com
	hostname := versionComponents[0] + "-dot-" + module + "-dot-" + appID + ".appspot.com"
	return hostname, nil
}
