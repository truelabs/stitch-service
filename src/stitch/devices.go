package stitch

import (
	"golang.org/x/net/context"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/memcache"
)

// Device is a device identifier and password. This is not a user controlled password.
type Device struct {
	ID          string `json:"id" datastore:"-"`
	Password    string `json:"password"`
	Username    string `json:"username"`
	DeviceToken string `json:"device_token"`
}

// The datastore Kind for a Device
const DeviceKind = "Device"

// DeviceKeyForID returns the datastore key for a device id.
func DeviceKeyForID(c context.Context, id string) (*datastore.Key, error) {
	key := datastore.NewKey(c, DeviceKind, id, 0, nil)
	if key.Incomplete() {
		return nil, ErrIncompleteKey
	}
	return key, nil
}

// Put returns ErrForbidden if session not authorized.
func (d *Device) Put(c context.Context) error {

	// Only create the entry once. At that point it's immutable and everyone, including the
	// device that created the record, is forbidden to access it.

	key, err := DeviceKeyForID(c, d.ID)
	if err != nil {
		return err
	}

	if len(d.ID) > MaxClientIDLen {
		log.Infof(c, "id length exceeded limit %v", len(d.ID))
		return ErrInvalidInput
	}

	if len(d.Password) > MaxClientPasswordLen || len(d.Password) < MinClientPasswordLen {
		log.Infof(c, "password length out of limit %v", len(d.Password))
		return ErrInvalidInput
	}

	err = datastore.RunInTransaction(c, func(c context.Context) error {
		existingDevice, err := GetDeviceByID(c, d.ID)
		if err == nil {
			// Have existing device. Can only update if passwords match.
			if existingDevice.Password != d.Password {
				return ErrForbidden
			}
		} else if err != datastore.ErrNoSuchEntity {
			return err
		}
		_, err = datastore.Put(c, key, d)
		return err
	}, nil)

	if err != nil {
		d.putMemcache(c)
	}

	return err
}

func (device *Device) putMemcache(c context.Context) {
	deviceNoID := *device
	// Save memcache space by not reduntantly storing the ID.
	deviceNoID.ID = ""
	err := memcache.JSON.Set(c, &memcache.Item{Key: device.ID, Object: deviceNoID})
	if err != nil {
		log.Warningf(c, "Error putting device into memcache. %v", err)
	}
}

// GetDeviceByID returns the device for the given id. First memcache is checked, and
// if there is a cache miss, the datastore is hit.
func GetDeviceByID(c context.Context, id string) (*Device, error) {

	device := new(Device)

	// Look for the device in memcache
	if _, err := memcache.JSON.Get(c, id, device); err == nil {
		device.ID = id
		return device, nil
	} else if err != memcache.ErrCacheMiss {
		log.Warningf(c, "memcache fail: device: %v", err)
	}

	// Not found in memecache, now hit datastore.
	key, err := DeviceKeyForID(c, id)
	if err != nil {
		return nil, err
	}
	err = datastore.Get(c, key, device)
	if err != nil {
		return nil, err
	}

	device.ID = id

	device.putMemcache(c)

	return device, nil
}

// GetDevicesByIDs returns the devices for the given ids. First memcache is checked, and
// if there is a cache miss, the datastore is hit.
func GetDevicesByIDs(c context.Context, ids []string) ([]Device, error) {

	// TODO: Use memcache here.

	/*
		notInMemcache := make([]string, 0, len(ids))

		//
		// Look for the device in memcache
		//
		cachedItems, err := memcache.GetMulti(c, ids)
		if err != nil {
			log.Warningf(c, "memcache get multi fail: device: %v", err)
		}
		cachedDevices := make([]Device, 0, len(cachedItems))
		for i, cachedItem := range cachedItems {
			var device Device
			err := json.Unmarshal(cachedItem.Value, &device)
			if err == nil {
				cachedDevices = append(cachedDevices, device)
			} else {
				log.Warningf(c, "Error unmarshalling memcached JSON. %v", err)
			}
		}

	*/

	//
	// Look in the datastore for the rest of the items.
	//

	// Get datastore keys for the items not found in memcache.
	var keys []*datastore.Key
	for _, id := range ids {
		/*
			if _, ok := cachedItems[id]; ok {
				// cached, don't need to lookup
				break
			}
		*/
		key, err := DeviceKeyForID(c, id)
		if err != nil {
			log.Errorf(c, "DeviceKeyForID returned error for key %v. %v", id, err)
			return nil, err
		}
		keys = append(keys, key)
	}

	devices := make([]Device, len(keys))
	err := datastore.GetMulti(c, keys, devices)
	if err != nil {
		log.Errorf(c, "Error GetMulti failed to get devices. %v", err)
		return nil, err
	}

	// Save back to memcache.
	/*
		if err := memcache.JSON.Set(c, &memcache.Item{Key: id, Object: device}); err != nil {
			log.Warningf(c, "Error putting device into memcache. %v", err)
		}
	*/

	for i := 0; i < len(keys); i++ {
		devices[i].ID = keys[i].StringID()
	}

	return devices, nil

}

// AuthenticateDevice returns true if the given device id has the given password.
func AuthenticateDevice(c context.Context, id string, password string) bool {
	existingDevice, err := GetDeviceByID(c, id)
	if err != nil {
		return false
	}
	return existingDevice.Password == password
}
