package stitch

import (
	"encoding/json"
	"github.com/drichardson/appengine/signedrequest"
	"github.com/drichardson/appengine/storage"
	"golang.org/x/net/context"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/taskqueue"
)

// MovieMaker defines the JSON task queue payload used to schedule a
// movie building request with the moviemaker service.
type MovieMaker struct {
	Assets       []storage.BucketObject `json:"assets"`
	OutputBucket string                 `json:"output_bucket"`

	// hex encoded content md5 is appended to this when writing the result.
	ObjectNamePrefix string `json:"object_name_prefix"`

	Callback signedrequest.SignedRequest `json:"callback"`
}

// QueueJob adds a movie maker request to the moviemaker task queue.
func (movie *MovieMaker) QueueJob(c context.Context) error {
	movieBytes, err := json.Marshal(movie)
	if err != nil {
		log.Errorf(c, "Error marshalling json for make movie spec. %v", err)
		return err
	}

	task := &taskqueue.Task{
		Method:  "PULL",
		Payload: movieBytes,
	}

	_, err = taskqueue.Add(c, task, "makemovie")
	if err != nil {
		log.Errorf(c, "Error adding task to makemovie queue. %v", err)
		return err
	}

	return nil
}
