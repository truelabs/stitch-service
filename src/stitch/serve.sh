#!/bin/bash
# Run a local development server using a Google Service Account for accessing
# live Google APIs.

usage() {
    echo "Usage: ./serve.sh <service_account_email> <service_account_pem_file> <dev_appserver.py options...>"
    echo
    echo "You can create a new service account by going to"
    echo "https://console.developers.google.com/ and then APIs & auth > Credentials"
    echo "When you create a service account, a private PKCS 12 (.p12) file will be"
    echo "downloaded. Convert this to a PEM file using openssl and then run this command."
    echo
    echo "You can also pass dev_appserver.py options. Here are some you might like:"
    echo "--datastore_consistency_policy consistent"
}

EMAIL=$1
shift
PRIVKEY_PEMFILE=$1
shift

if [ -z "$EMAIL" ]; then
    echo "Missing service_account_email"
    usage
    exit 1
fi

if [ -z "$PRIVKEY_PEMFILE" ]; then
    echo "Missing service_account_pem_file"
    usage
    exit 1
fi

dev_appserver.py --appidentity_email_address $EMAIL --appidentity_private_key_path "$PRIVKEY_PEMFILE" $@ ./app.yaml
