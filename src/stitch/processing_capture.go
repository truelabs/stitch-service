package stitch

import (
	"github.com/drichardson/appengine/storage"
	"golang.org/x/net/context"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"time"
)

// ProcessingCaptureKind is the datastore Kind
const ProcessingCaptureKind = "ProcessingCapture"

// ProcessingCapture maintains the state of a Capture that is in process and
// not yet ready to be associated with an event's capture list. This may be,
// for example, because it's waiting for video transcoding.
// This helps to simplify the Capture's in events because those are immutable
// while ProcessingCapture's Captures are mutable.
type ProcessingCapture struct {

	// The Capture being processed.
	Capture Capture

	// Time the capture was last updated by some processing event.
	LastUpdated time.Time

	// Extracted frame, for video.
	ExtractedVideoFrames []VideoFrame

	// Expected transcode objects. Used to determine how far along capture processing is.
	ExpectedTranscodeObjects []storage.BucketObject

	// Expected video frame objects. Used to determine how far along capture processing is.
	ExpectedVideoFrameObjects []storage.BucketObject
}

// NewProcessingCapture returns a new ProcessingCapture object identified by (eventID, owner, contentMD5).
func NewProcessingCapture(c context.Context, eventID, owner, contentMD5 string) (*ProcessingCapture, error) {
	capture, err := NewCapture(c, eventID, owner, contentMD5)
	if err != nil {
		return nil, err
	}
	pc := &ProcessingCapture{
		Capture: *capture,
	}
	return pc, nil
}

func bucketObjectInVideos(bo storage.BucketObject, videos []Video) bool {
	for _, v := range videos {
		if bo == v.Object {
			return true
		}
	}
	return false
}

func bucketObjectInVideoFrames(bo storage.BucketObject, frames []VideoFrame) bool {
	for _, v := range frames {
		if bo == v.Object {
			return true
		}
	}
	return false
}

// ExpectedObjectsExist returns true if all expected objects appear in the capture.
func (pc *ProcessingCapture) ExpectedObjectsExist() bool {
	for _, expected := range pc.ExpectedTranscodeObjects {
		if !bucketObjectInVideos(expected, pc.Capture.Transcodes) {
			return false
		}
	}
	for _, expected := range pc.ExpectedVideoFrameObjects {
		if !bucketObjectInVideoFrames(expected, pc.ExtractedVideoFrames) {
			return false
		}
	}

	return true
}

// ProcessingCaptureKey identifies a ProcessingCapture. They are used to avoid unnecessary datastore
// Gets, since ProcessingCaptures generally accumulate (append) information and less often
// need to return the ProcessingCapture to the application.
type ProcessingCaptureKey struct {
	EventID    string
	OwnerID    string
	ContentMD5 string
}

// NewProcessingCaptureKey returns a key that identifies a ProcessCapture.
func NewProcessingCaptureKey(eventID, ownerID, contentMD5 string) *ProcessingCaptureKey {
	return &ProcessingCaptureKey{EventID: eventID, OwnerID: ownerID, ContentMD5: contentMD5}
}

func (pcKey *ProcessingCaptureKey) datastoreKey(c context.Context) (*datastore.Key, error) {
	keyStr := pcKey.EventID + "/" + pcKey.OwnerID + "/" + pcKey.ContentMD5
	key := datastore.NewKey(c, ProcessingCaptureKind, keyStr, 0, nil)
	if key.Incomplete() {
		return nil, ErrIncompleteKey
	}
	return key, nil
}

// GetProcessingCapture returns the ProcessingCapture for the given key tuple.
// Returns datastore.ErrNoSuchEntity if not found.
func (pcKey *ProcessingCaptureKey) GetProcessingCapture(c context.Context) (*ProcessingCapture, error) {
	key, err := pcKey.datastoreKey(c)
	if err != nil {
		return nil, err
	}
	pc := new(ProcessingCapture)
	err = datastore.Get(c, key, pc)
	if err != nil {
		return nil, err
	}
	return pc, nil
}

// put writes the ProcessingCapture to the datastore. If an entry for this ProcessingCapture
// exists, then the resolve function is called with a non-nil *ProcessCapture. The update
// function is expected to modify the a *ProcessCapture, which will then be written
// back to the datastore.
func (pcKey *ProcessingCaptureKey) put(c context.Context, update func(c context.Context, pc *ProcessingCapture) error) (*ProcessingCapture, error) {
	key, err := pcKey.datastoreKey(c)
	if err != nil {
		return nil, err
	}

	var result *ProcessingCapture
	err = datastore.RunInTransaction(c, func(c context.Context) error {
		pc, err := NewProcessingCapture(c, pcKey.EventID, pcKey.OwnerID, pcKey.ContentMD5)
		if err != nil {
			log.Errorf(c, "NewProcessingCapture failed. %v", err)
			return err
		}
		err = datastore.Get(c, key, pc)
		if err != nil && err != datastore.ErrNoSuchEntity {
			log.Errorf(c, "datastore.Get failed to get processing entry. %v", err)
			return err
		}
		err = update(c, pc)
		if err != nil {
			log.Errorf(c, "update failed with %v", err)
			return err
		}
		pc.LastUpdated = time.Now()
		_, err = datastore.Put(c, key, pc)
		if err != nil {
			return err
		}

		result = pc
		return nil
	}, nil)

	if err != nil {
		return nil, err
	}

	return result, nil
}

func videoInSlice(v Video, slice []Video) bool {
	for _, e := range slice {
		if e == v {
			return true
		}
	}
	return false
}

func mergeUniqueVideos(v1 []Video, v2 []Video) []Video {
	result := make([]Video, len(v1))
	copy(result, v1)
	for _, v := range v2 {
		if !videoInSlice(v, v1) {
			result = append(result, v)
		}
	}
	return result
}

func videoFrameInSlice(v VideoFrame, slice []VideoFrame) bool {
	for _, e := range slice {
		if e == v {
			return true
		}
	}
	return false
}

func mergeUniqueVideoFrames(v1 []VideoFrame, v2 []VideoFrame) []VideoFrame {
	result := make([]VideoFrame, len(v1))
	copy(result, v1)
	for _, v := range v2 {
		if !videoFrameInSlice(v, v1) {
			result = append(result, v)
		}
	}
	return result
}

// AddTranscodesAndFrames appends the transcodes and extractedFrames to the ProcessingCapture identified
// by this key. Only unique transcodes and frames are added (duplicates are not). The updated capture
// is returned on success.
func (key *ProcessingCaptureKey) AddTranscodesAndFrames(c context.Context, transcodes []Video, extractedFrames []VideoFrame) (*ProcessingCapture, error) {
	return key.put(c, func(c context.Context, pc *ProcessingCapture) error {
		pc.Capture.Transcodes = mergeUniqueVideos(pc.Capture.Transcodes, transcodes)
		pc.ExtractedVideoFrames = mergeUniqueVideoFrames(pc.ExtractedVideoFrames, extractedFrames)
		return nil
	})
}

// PutExpectedTranscodeAndVideoFrameObjects writes the expectedTranscodeObjects and
// expectedVideoFrameObjects to the ProcessingCapture identified by this key. Any
// existing entries are overwritten.
func (key *ProcessingCaptureKey) PutExpectedTranscodeAndVideoFrameObjects(c context.Context, expectedTranscodeObjects []storage.BucketObject, expectedVideoFrameObjects []storage.BucketObject) error {
	_, err := key.put(c, func(c context.Context, pc *ProcessingCapture) error {
		if count := len(pc.ExpectedTranscodeObjects); count != 0 {
			log.Warningf(c, "Overriding %v existing expected transcode objects.", count)
		}
		if count := len(pc.ExpectedVideoFrameObjects); count != 0 {
			log.Warningf(c, "Overriding %v existing expected video frame objects.", count)
		}
		pc.ExpectedTranscodeObjects = expectedTranscodeObjects
		pc.ExpectedVideoFrameObjects = expectedVideoFrameObjects
		return nil
	})

	return err
}

// DeleteProcessingCapture deletes the ProcessingCapture identified by pcKey.
func DeleteProcessingCapture(c context.Context, pcKey *ProcessingCaptureKey) error {
	log.Debugf(c, "Deleting processing capture for pcKey %v", pcKey)
	key, err := pcKey.datastoreKey(c)
	if err != nil {
		log.Errorf(c, "Error getting datastore key from pc key. %v", err)
		return err
	}

	err = datastore.Delete(c, key)
	if err != nil {
		log.Errorf(c, "Error deleting processing capture. %v", err)
		return err
	}

	return nil
}
