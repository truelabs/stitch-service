package stitch

import (
	"crypto/md5"
	"golang.org/x/net/context"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"time"
)

// CaptureKind is the datastore Kind
const CaptureKind = "Capture"
const deletedCaptureKind = "DeletedCapture"

// Capture identifies the owner and content for an item uploaded to an event.
type Capture struct {
	// The event this capture is associated with.
	EventKey *datastore.Key

	// The device that owns this capture.
	OwnerKey *datastore.Key

	// Hex encoded MD5 of the content of the original capture.
	ContentMD5 string

	// The time the Capture was saved to the datastore.
	Time time.Time

	// Content-Type (MIME type) of the capture.
	ContentType string

	// Thumbnails generated from the image or from a frame extracted from video.
	Thumbnails []Thumbnail

	// Lower bit-rate versions of the original video (or possibly just different codecs).
	Transcodes []Video
}

// NewCapture validates the arguments and returns a new Capture.
func NewCapture(c context.Context, eventID, owner, contentMD5 string) (*Capture, error) {
	eventKey, err := EventKeyForID(c, eventID)
	if err != nil {
		return nil, err
	}
	ownerKey, err := DeviceKeyForID(c, owner)
	if err != nil {
		return nil, err
	}
	const hexEncodedMD5Len = md5.Size * 2
	if len(contentMD5) != hexEncodedMD5Len {
		log.Infof(c, "Invalid MD5 length %v. Expected %v", len(contentMD5), hexEncodedMD5Len)
		return nil, ErrInvalidInput
	}
	capture := &Capture{
		EventKey:   eventKey,
		OwnerKey:   ownerKey,
		ContentMD5: contentMD5,
	}
	return capture, nil
}

func datastoreKeyFromParts(c context.Context, kind, eventKey, ownerKey, contentMD5 string) (*datastore.Key, error) {
	keyString := eventKey + "/" + ownerKey + "/" + contentMD5
	key := datastore.NewKey(c, kind, keyString, 0, nil)
	if key.Incomplete() {
		return nil, ErrIncompleteKey
	}
	return key, nil
}

func (capture *Capture) datastoreKeyInternal(c context.Context, kind string) (*datastore.Key, error) {
	return datastoreKeyFromParts(c, kind, capture.EventKey.StringID(), capture.OwnerKey.StringID(), capture.ContentMD5)
}

// DatastoreKey returns the datastore.Key that will be used to identify this Capture.
func (capture *Capture) DatastoreKey(c context.Context) (*datastore.Key, error) {
	return capture.datastoreKeyInternal(c, CaptureKind)
}

// Put the capture in the datastore.
func (capture *Capture) Put(c context.Context) error {
	key, err := capture.DatastoreKey(c)
	if err != nil {
		return err
	}

	capture.Time = time.Now()

	err = datastore.RunInTransaction(c, func(c context.Context) error {
		existingCapture := new(Capture)
		err := datastore.Get(c, key, existingCapture)
		if err == datastore.ErrNoSuchEntity {
			_, err = datastore.Put(c, key, capture)
			return err
		} else if err != nil {
			log.Errorf(c, "Error looking up %v in datastore. %v", key, err)
			return err
		} else {
			// There's an existing entry. Do nothing, because we don't want to change the upload time.
			log.Debugf(c, "Existing capture, ignoring Put.")
			return nil
		}
	}, nil)

	log.Debugf(c, "Put capture object %v, err %v", key, err)
	return err
}

// Delete marks a CaptureKind capture for deletion by putting in a deletedCaptureKind.
func DeleteCapture(c context.Context, eventID, ownerID, contentMD5 string) error {
	originalCaptureKey, err := datastoreKeyFromParts(c, CaptureKind, eventID, ownerID, contentMD5)
	if err != nil {
		return err
	}

	deletedCaptureKey, err := datastoreKeyFromParts(c, deletedCaptureKind, eventID, ownerID, contentMD5)
	if err != nil {
		return err
	}

	return datastore.RunInTransaction(c, func(c context.Context) error {
		capture := new(Capture)
		err := datastore.Get(c, originalCaptureKey, capture)
		if err != nil {
			log.Errorf(c, "Error looking up original capture while trying to delete. %v", err)
			return err
		}
		_, err = datastore.Put(c, deletedCaptureKey, capture)
		if err != nil {
			log.Errorf(c, "Error putting capture in deleted kind. %v", err)
			return err
		}
		err = datastore.Delete(c, originalCaptureKey)
		if err != nil {
			log.Errorf(c, "Error deleting capture. %v", err)
			return err
		}
		return nil
	}, &datastore.TransactionOptions{XG: true})
}

// GetCapturesByEventID returns all Capture objects associated with the event.
func GetCapturesByEventID(c context.Context, eventID string) ([]Capture, error) {
	var captures = make([]Capture, 0)
	eventKey, err := EventKeyForID(c, eventID)
	if err != nil {
		return captures, err
	}
	_, err = datastore.NewQuery(CaptureKind).Filter("EventKey =", eventKey).GetAll(c, &captures)
	return captures, err
}
