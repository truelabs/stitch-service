# Stitch Service
Components that make up the stitch service. 

All commands require GOPATH be set to the directory that containst this README file.

    cd <this_directory>
    export GOPATH=$(pwd)

The development server also requires the stitch bucket to be created in a particular
way. You can create it like this:

    cd $GOPATH/src/stitch
    ./make_stich_bucket.sh

## Running Development Server
There are two ways to run the development server, `goapp` or `serve.sh`. `serve.sh`
runs a local development server, but also lets you specify a Google Service Account
to access services like Google Cloud Storage. You need to use `serve.sh` when running
tests against the local development instance.

To run `serve.sh`:

    cd $GOPATH/src/stitch
    go get -v
    ./serve.sh

To run `goapp`:

    cd $GOPATH/src/stitch
    goapp serve
    
## Running Tests
This will run the tests against the development server, but note it is using the real Google Cloud Storage
and other Google services, assuming you provided a service account to the `serve.sh` script above.

    cd $GOPATH/src/tests
    go test

To run tests against a deployed server, run:

    cd $GOPATH/src/tests
    go test -apibase="https://stitch-1026.appspot.com

## Deploying to App Engine

    cd $GOPATH/src/stitch
    goapp deploy

## Vim Go Development Environment for App Engine
These steps will enable autocompletion and lookups (:GoDef) for App Engine symbols.

1. Install vim-go
2. Build the vim-go helper tools and install to their own dedicated folder:

    mkdir $HOME/vim-go
    env GOPATH=$HOME/vim-go vim +GoInstallBinaries +qall

3. Add this to your .vimrc:

    let g:go_bin_path = expand("~/vim-go/bin")

4. Build the project so autocomplete can perform lookups. Note: you have to redo this step whenever new projects/symbols are added.

    go install stitch

